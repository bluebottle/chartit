Portfolio management written in go using opengl GUI and postgres database.

Installation:

- Add config file
The file etc/chartit/chartit.yml.tmpl is a template for the configuration and should be placed as /etc/chartit/chartit.yml on the system, containing the parameters for your database.

- Database creation
The shell skript databases/chartit_install.sh can be used to do that

- Demo data
The databases/demo* files can be used to import demo values. To do so you need to edit the *.txt files and  subtitute <path> with the full path where the csv files reside. After that, the demo_import.sh file can be started to import the data.

There is no demo data for quotes, as quotes for the lookup values can be downloaded via the batch/updateloop program.
