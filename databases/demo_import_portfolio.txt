\connect chartit;
\echo ---Import portfolio data
COPY public.portfolio(portfolio_id,name,trade_risk,port_risk) 
FROM '<path>/demo_portfolio.csv'
WITH NULL as 'NULL'
DELIMITER ','
CSV HEADER;
