\echo ---Drop old database
DROP DATABASE IF EXISTS chartit;

\echo ---Create new database and connect to it
CREATE DATABASE chartit
		ENCODING = 'UTF8'
		LC_COLLATE = 'en_US.utf8'
		LC_CTYPE = 'en_US.utf8'
		TABLESPACE = pg_default
		OWNER = sicota
		TEMPLATE template0;
\connect chartit

-- Contains all the currencies used by the symbols
CREATE TABLE public.currencies (
  currency text NOT NULL,
  currencycountry text NOT NULL,
  iso_code text NOT NULL,
  symbol text NOT NULL,
  CONSTRAINT curr_isolen     CHECK (char_length(iso_code::text) <= 3),
  CONSTRAINT sym_isolen      CHECK (char_length(symbol::text) <= 3)
);
CREATE UNIQUE INDEX currencies_name_idx ON public.currencies USING btree (iso_code);

ALTER TABLE public.currencies OWNER TO sicota;

-- Contains references symbol of currency pair
CREATE TABLE public.curr_symb_cross (
  iso_code_src text NOT NULL,
  iso_code_dest text NOT NULL,
  symbol text NOT NULL,
  CONSTRAINT curr_isosrclen      CHECK (char_length(iso_code_src::text) <= 3),
  CONSTRAINT curr_isodestlen     CHECK (char_length(iso_code_dest::text) <= 3),
  CONSTRAINT fk_csx_symbol       FOREIGN KEY (symbol) REFERENCES public.lookup(symbol),
  CONSTRAINT fk_csx_ics          FOREIGN KEY (iso_code_src) REFERENCES public.currencies(iso_code),
  CONSTRAINT fk_csx_icd          FOREIGN KEY (iso_code_dest) REFERENCES public.currencies(iso_code)
);
CREATE UNIQUE INDEX curr_symb_cross_idx ON public.curr_symb_cross USING btree (iso_code_src, iso_code_dest);

ALTER TABLE public.curr_symb_cross OWNER TO sicota;

-- Contains the portfolios
CREATE TABLE IF NOT EXISTS public.portfolio (
  portfolio_id       int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
  "name"             text COLLATE pg_catalog."default" NOT NULL,
  trade_risk         numeric(5,2) NOT NULL DEFAULT 1.0,
  port_risk          numeric(5,2) NOT NULL DEFAULT 10.0,
  CONSTRAINT portfolio_pkey PRIMARY KEY (portfolio_id),
  CONSTRAINT port_namelen      CHECK (char_length("name"::text) <= 40)
);

ALTER TABLE public.portfolio OWNER TO sicota;

-- Contains the depositions and withdrawals for each portfolio
CREATE TYPE flow_direction AS ENUM ('Deposit', 'Withdrawal');

CREATE TABLE IF NOT EXISTS public.portfolio_moneyflow (
	portfolio_id      int8 NOT NULL DEFAULT 0,
	flow_date         timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
	port_direction    flow_direction NOT NULL DEFAULT 'Deposit',
	flow_amount       numeric(30,10) NOT NULL DEFAULT 0.0,
	CONSTRAINT moneyflow_pkey PRIMARY KEY (portfolio_id),
	CONSTRAINT fk_trades_portfolio FOREIGN KEY (portfolio_id) REFERENCES public.portfolio(portfolio_id) ON DELETE CASCADE ON UPDATE CASCADE
);

ALTER TABLE public.portfolio_moneyflow OWNER TO sicota;

\echo ---Create lookup table
-- Contains the information for all symbols
CREATE TYPE types AS ENUM ('Currency', 'Stock', 'Bond Fund', 'Ethical Fund', 'Funds of funds', 'Index Fund', 'International Fund', 'Money Market Fund', 'Regional Fund', 'Sector Fund', 'Index', 'Sector', 'Warrant', 'ETF', 'Managed Fund', 'Commodity', 'Testindex', 'Discount Certificate', 'Turbo Certificate', 'Other');
CREATE TYPE cap AS ENUM ('Micro', 'Small', 'Mid', 'Large');

CREATE TABLE IF NOT EXISTS public.lookup (
  symbol             text UNIQUE COLLATE pg_catalog."default" NOT NULL DEFAULT '',
  "name"             text COLLATE pg_catalog."default" NOT NULL DEFAULT '',
  wkn                text COLLATE pg_catalog."default" NOT NULL DEFAULT '',
  isin               text COLLATE pg_catalog."default" NOT NULL DEFAULT '',
  look_type          types NOT NULL DEFAULT 'Stock',
  quote_currency     text COLLATE pg_catalog."default" NOT NULL DEFAULT 'EUR',
  is_history         boolean NOT NULL DEFAULT false,
  CONSTRAINT lookup_pkey PRIMARY KEY (symbol),
  CONSTRAINT lookup_curr FOREIGN KEY (quote_currency) REFERENCES public.currencies(iso_code) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT look_symlen     CHECK (char_length(symbol::text) <= 50),
  CONSTRAINT look_namelen    CHECK (char_length("name"::text) <= 255),
  CONSTRAINT look_wknlen     CHECK (char_length(wkn::text) <= 10),
  CONSTRAINT look_isinlen    CHECK (char_length(isin::text) <= 30),
  CONSTRAINT look_quotelen   CHECK (char_length(quote_currenxy::text) <= 10)
);

ALTER TABLE public.lookup OWNER TO sicota;
CREATE UNIQUE INDEX i_lookup_symbol ON public.lookup (symbol);
CREATE INDEX i_lookup_type ON public.lookup (look_type);
CREATE INDEX i_lookup_wkn ON public.lookup (wkn);
CREATE UNIQUE INDEX i_lookup_isin ON public.lookup (isin);
CREATE INDEX i_lookup_history ON public.lookup (is_history);

\echo ---Create quote table
-- Contains all the quotes for the symbols
CREATE TABLE IF NOT EXISTS public.quote (
  symbol             text COLLATE pg_catalog."default" NOT NULL DEFAULT '',
  quote_date         timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  open               numeric(10,3) NOT NULL DEFAULT 0.0,
  high               numeric(10,3) NOT NULL DEFAULT 0.0,
  low                numeric(10,3) NOT NULL DEFAULT 0.0,
  close              numeric(10,3) NOT NULL DEFAULT 0.0,
  volume             bigint NOT NULL DEFAULT 0,
  CONSTRAINT quote_pkey            PRIMARY KEY (symbol,quote_date),
  CONSTRAINT quote_open_check      CHECK (open   >= 0.0),
  CONSTRAINT quote_high_check      CHECK (high   >= 0.0),
  CONSTRAINT quote_highlow_check   CHECK (high   >= low),
  CONSTRAINT quote_highopen_check  CHECK (high   >= open),
  CONSTRAINT quote_highclose_check CHECK (high   >= close),
  CONSTRAINT quote_low_check       CHECK (low    >= 0.0),
  CONSTRAINT quote_lowhigh_check   CHECK (low    <= high),
  CONSTRAINT quote_lowopen_check   CHECK (low    <= open),
  CONSTRAINT quote_lowclose_check  CHECK (low    <= close),
  CONSTRAINT quote_close_check     CHECK (close  >= 0.0),
  CONSTRAINT quote_volume_check    CHECK (volume >= 0),
  CONSTRAINT quote_symlen          CHECK (char_length(symbol::text) <= 50)
);

ALTER TABLE public.quote OWNER TO sicota;

ALTER TABLE public.quote
    ADD CONSTRAINT fk_quote_symbol FOREIGN KEY (symbol)
    REFERENCES public.lookup (symbol) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE CASCADE;

-- Contains all the trades for the symbols
CREATE TYPE stopLossType AS ENUM ('%', 'Fixed', 'None');

CREATE TABLE IF NOT EXISTS public.trades (
	trade_id          bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
	portfolio_id      int8 NOT NULL DEFAULT 0,
	symbol            text NOT NULL DEFAULT ''::text,
	"comment"         text NOT NULL DEFAULT ''::text,
	check_level_value numeric(10,3) NOT NULL DEFAULT 0.0,
	stop_loss_type    stopLossType NOT NULL DEFAULT 'None'::stopLossType,
	stop_loss_value   numeric(10,3) NOT NULL DEFAULT 0.0,
	CONSTRAINT trades_un           UNIQUE (trade_id),
	CONSTRAINT fk_trades_portfolio FOREIGN KEY (portfolio_id) REFERENCES public.portfolio(portfolio_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_trades_symbol    FOREIGN KEY (symbol) REFERENCES public.lookup(symbol) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT trade_symlen          CHECK (char_length(symbol::text) <= 50),
	CONSTRAINT trade_comlen          CHECK (char_length("comment"::text) <= 255)
);
CREATE INDEX id_trades ON public.trades USING btree (portfolio_id);
CREATE UNIQUE INDEX i_trades_tradeid ON public.trades (trade_id);

ALTER TABLE public.trades OWNER TO sicota;

CREATE TABLE IF NOT EXISTS public.tradehistory (
	trade_id          bigint NOT NULL,
	portfolio_id      int8 NOT NULL DEFAULT 0,
	symbol            text NOT NULL DEFAULT ''::text,
	"comment"         text NOT NULL DEFAULT ''::text,
	check_level_value numeric(10,3) NOT NULL DEFAULT 0.0,
	stop_loss_type    stopLossType NOT NULL DEFAULT 'None'::stopLossType,
	stop_loss_value   numeric(10,3) NOT NULL DEFAULT 0.0,
	CONSTRAINT tradehist_un           UNIQUE (trade_id),
	CONSTRAINT fk_tradehist_portfolio FOREIGN KEY (portfolio_id) REFERENCES public.portfolio(portfolio_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_tradehist_symbol    FOREIGN KEY (symbol) REFERENCES public.lookup(symbol) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT tradehist_symlen          CHECK (char_length(symbol::text) <= 50),
	CONSTRAINT tradehist_comlen          CHECK (char_length("comment"::text) <= 255)
);
CREATE INDEX id_tradehist ON public.tradehistory USING btree (portfolio_id);
CREATE UNIQUE INDEX i_tradehist_tradeid ON public.tradehistory (trade_id);

ALTER TABLE public.tradehistory OWNER TO sicota;

-- Contains all the transactions for the symbols
CREATE TYPE actions AS ENUM ('Buy', 'Dividend', 'Sale', 'Split', 'Swap');

CREATE TABLE IF NOT EXISTS public.transactions (
	trans_id     int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	trade_id     int8 NOT NULL,
	portfolio_id int8 NOT NULL DEFAULT 0,
	symbol       text NOT NULL DEFAULT ''::text,
	"date"       timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
	"action"     actions NOT NULL DEFAULT 'Buy'::actions,
	price        numeric(10, 3) NOT NULL DEFAULT 0.0,
	quantity     numeric(10, 3) NOT NULL DEFAULT 0.0,
	brokerage    numeric(10, 3) NOT NULL DEFAULT 0.0,
	slippage     numeric(10, 3) NOT NULL DEFAULT 0.0,
	CONSTRAINT transactions_un        UNIQUE (trans_id),
	CONSTRAINT transactions_lookup    FOREIGN KEY (symbol) REFERENCES public.lookup(symbol) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT transactions_portfolio FOREIGN KEY (portfolio_id) REFERENCES public.portfolio(portfolio_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT transactions_trades    FOREIGN KEY (trade_id) REFERENCES public.trades(trade_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT trans_symlen           CHECK (char_length(symbol::text) <= 50)
);

ALTER TABLE public.transactions OWNER TO sicota;

-- Contains all the warrants for the symbols
CREATE TABLE public.warrant (
	symbol           text NOT NULL,
	base_symbol      text NOT NULL,
	warrant_end_date date NOT NULL,
	CONSTRAINT warrant_base_lookup FOREIGN KEY (base_symbol) REFERENCES public.lookup(symbol) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT warrant_lookup      FOREIGN KEY (symbol) REFERENCES public.lookup(symbol) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT warr_symlen           CHECK (char_length(symbol::text) <= 50),
	CONSTRAINT warr_basesymlen       CHECK (char_length(base_symbol::text) <= 50)
);
CREATE UNIQUE INDEX warrant_symbol_idx ON public.warrant USING btree (symbol);

ALTER TABLE public.warrant OWNER TO sicota;

-- Contains all the watchlists
CREATE TABLE public.watchlists (
	watchlist_id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	watch_name text NOT NULL,
	CONSTRAINT watch_namelen       CHECK (char_length(watch_name::text) <= 255)
);
CREATE UNIQUE INDEX watchlists_name ON public.watchlists USING btree (watch_name);
CREATE UNIQUE INDEX watchlists_id ON public.watchlists USING btree (watchlist_id);

ALTER TABLE public.watchlists OWNER TO sicota;

-- Contains all the watchlist content
CREATE TABLE public.watchlist_content (
	watchlist_id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	symbol       text NOT NULL,
	CONSTRAINT watchlist_content_symbol FOREIGN KEY (symbol) REFERENCES public.lookup(symbol) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT watchlist_content_id	FOREIGN KEY (watchlist_id) REFERENCES public.watchlists(watchlist_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT watcon_symlen       CHECK (char_length(symbol::text) <= 50)
);
CREATE UNIQUE INDEX watchlist_content_idx ON public.watchlist_content USING btree (watchlist_id);

ALTER TABLE public.watchlist_content OWNER TO sicota;

-- Contains all the values for risk evaluation (discount rate)
CREATE TABLE public.risk (
	risk_id             int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	country             text NOT NULL,
  market_risk_premium numeric(10, 3) NOT NULL DEFAULT 0.0,
  risk_free_rate      numeric(10, 3) NOT NULL DEFAULT 0.0,
	CONSTRAINT risk_country CHECK (char_length(country::text) <= 255)
);
CREATE UNIQUE INDEX risk_country_idx ON public.risk USING btree (country);

ALTER TABLE public.risk OWNER TO sicota;
