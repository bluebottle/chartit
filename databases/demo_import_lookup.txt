\connect chartit;
\echo ---Import lookup data
COPY public.lookup(symbol,name,wkn,isin,sector,look_type,quote_currency,is_history) 
FROM '<path>/demo_lookup.csv'
WITH NULL as 'NULL'
DELIMITER ','
CSV HEADER;
