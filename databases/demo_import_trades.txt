\connect chartit;
\echo ---Import trades data
COPY public.trades(portfolio_id,symbol,comment,check_level_value,stop_loss_type,stop_loss_value)
FROM '<path>/demo_trades.csv'
WITH NULL as 'NULL'
DELIMITER ','
CSV HEADER;
