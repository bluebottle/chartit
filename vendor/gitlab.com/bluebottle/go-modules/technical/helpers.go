// Package technical Some functions used for my trading projects, like indicators and portfolio functions
package technical

import "gitlab.com/bluebottle/go-modules/postgresql"

func highLow(values []postgresql.Quote) (tmpHigh, tmpLow float64) {
	for _, v := range values {
		if v.High > tmpHigh {
			tmpHigh = v.High
		} // if

		if v.Low < tmpLow || tmpLow == 0.0 {
			tmpLow = v.Low
		} // if
	} // for

	return
} // highLow ()
