// Package technical Some functions used for my trading projects, like indicators and portfolio functions
package technical

import (
	"gitlab.com/bluebottle/go-modules/postgresql"
)

// FullStoch contains the stochastics for the set values
type FullStoch struct {
	KLine  []float64
	DLine  []float64
	Period int
	KValue int
	DValue int
}

// FullStochastic Calculating full stochastic oscillator
//
// %K = (Current Close - Lowest Low for the look-back period)/(Highest High for the look-back period - Lowest Low for the look-back period) * 100
// %D = 3-day SMA of %K
//
// Full Stochastic Oscillator:
// Full %K = Fast %K smoothed with X-period SMA
// Full %D = X-period SMA of Full %K
func (f *FullStoch) FullStochastic(values []postgresql.Quote) (tmpStoch FullStoch) {
	tmpStoch = *f

	for i := tmpStoch.Period; i <= len(values); i++ {
		tmpStoch.KLine = append(tmpStoch.KLine, basicStoch(values[i-tmpStoch.Period:i]))
	} // for

	// Smooth the k line (not for the fast stochastics)
	if tmpStoch.KValue > 0 {
		tmpStoch.KLine = SMA(tmpStoch.KValue, tmpStoch.KLine)
	} // if

	tmpStoch.DLine = SMA(tmpStoch.DValue, tmpStoch.KLine)
	return
} // FullStochastic ()

func basicStoch(values []postgresql.Quote) (basic float64) {
	tmpHigh, tmpLow := highLow(values)
	return ((values[len(values)-1].Close - tmpLow) / (tmpHigh - tmpLow)) * 100.0
} // basicStoch ()

// SlowStochastic Calculate the slow stochastics (3 day SMA for k-line and d-line)
// Slow Stochastic Oscillator:
// Slow %K = Fast %K smoothed with 3-period SMA
// Slow %D = 3-period SMA of Slow %K
func SlowStochastic(values []postgresql.Quote, period int) (stoch FullStoch) {
	stoch = FullStoch{Period: period, KValue: 3, DValue: 3}
	return stoch.FullStochastic(values)
} // SlowStochastic ()

// FastStochastic Calculate the fast stochastics (unsmoothed k-line, 3 day SMA for d-line)
// Fast Stochastic Oscillator:
// Fast %K = %K basic calculation
// Fast %D = 3-period SMA of Fast %K
func FastStochastic(values []postgresql.Quote, period int) (stoch FullStoch) {
	stoch = FullStoch{Period: period, KValue: 0, DValue: 3}
	return stoch.FullStochastic(values)
} // FastStochastic ()

// CompleteCurrentStochastic calculates the complete current stochastics
func CompleteCurrentStochastic(symbol string, period, k, d int, dayLevel, weekLevel, monthLevel, yearLevel float64) (evalValues map[string]Evaluate) {
	evalValues = make(map[string]Evaluate)

	quoteVal := postgresql.FindQuotes(symbol, "asc")
	stoch := FullStoch{Period: 14, KValue: 5, DValue: 5}
	DFS := stoch.FullStochastic(quoteVal)

	weekValues := postgresql.WeeklyQuotes(quoteVal)
	stoch = FullStoch{Period: 14, KValue: 5, DValue: 5}
	WFS := stoch.FullStochastic(weekValues)

	monthValues := postgresql.MonthlyQuotes(quoteVal)
	stoch = FullStoch{Period: 14, KValue: 5, DValue: 5}
	MFS := stoch.FullStochastic(monthValues)

	yearValues := postgresql.YearlyQuotes(quoteVal)
	stoch = FullStoch{Period: 14, KValue: 5, DValue: 5}
	YFS := stoch.FullStochastic(yearValues)

	evalValues["D"] = EvaluateValues(DFS.DLine, dayLevel)
	evalValues["W"] = EvaluateValues(WFS.DLine, weekLevel)
	evalValues["M"] = EvaluateValues(MFS.DLine, monthLevel)
	evalValues["Y"] = EvaluateValues(YFS.DLine, yearLevel)

	return
} // CompleteCurrentStochastic ()
