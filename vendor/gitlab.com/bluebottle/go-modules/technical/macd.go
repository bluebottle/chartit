// Package technical Some functions used for my trading projects, like indicators and portfolio functions
package technical

import (
	"gitlab.com/bluebottle/go-modules/postgresql"
)

// MACD calculates the MACD for the given lookup values and ema period (d, w, m, y).
// Calculated as EMA(emaLow) - EMA(emaHigh) of lookup values, signal line ema(emaSignal) of macd
func MACD(lookupVal []postgresql.Lookup, symbol, emaPeriod string, emaLow, emaHigh, emaSignal int) (macd, signalLine []float64) {
	var emaLows, emaHighs []float64
	QuoteVal := postgresql.FindQuotes(symbol, "asc")

	// get both ema values
	switch emaPeriod {
	case "d":
		emaLows = EMA(emaLow, postgresql.GetClose(QuoteVal))
		emaHighs = EMA(emaHigh, postgresql.GetClose(QuoteVal))
	case "w":
		emaLows = EMA(emaLow, postgresql.GetClose(postgresql.WeeklyQuotes(QuoteVal)))
		emaHighs = EMA(emaHigh, postgresql.GetClose(postgresql.WeeklyQuotes(QuoteVal)))
	case "m":
		emaLows = EMA(emaLow, postgresql.GetClose(postgresql.MonthlyQuotes(QuoteVal)))
		emaHighs = EMA(emaHigh, postgresql.GetClose(postgresql.MonthlyQuotes(QuoteVal)))
	case "y":
		emaLows = EMA(emaLow, postgresql.GetClose(postgresql.YearlyQuotes(QuoteVal)))
		emaHighs = EMA(emaHigh, postgresql.GetClose(postgresql.YearlyQuotes(QuoteVal)))
	} // switch

	// // Calculate macd
	for i, j := range emaHighs {
		macd = append(macd, emaLows[i]-j)
	} // for

	// calculate signal line
	signalLine = EMA(emaSignal, macd)
	return
} // MACD ()
