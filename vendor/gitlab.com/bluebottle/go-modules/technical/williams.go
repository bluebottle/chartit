// Package technical Some functions used for my trading projects, like indicators and portfolio functions
package technical

import (
	"gitlab.com/bluebottle/go-modules/postgresql"
)

// WilliamsR contains the Williams%R values
type WilliamsR struct {
	WRValue []float64
}

// WilliamsRP calculates Williams R%
// R% = ((HN - C) / (HN - LN)) * -100
func WilliamsRP(values []postgresql.Quote, period int) (willR WilliamsR) {
	for i := period; i <= len(values); i++ {
		willR.WRValue = append(willR.WRValue, basicWilliams(values[i-period:i])*-1.0)
	} // for

	return
} // WilliamsRP ()

func basicWilliams(values []postgresql.Quote) (basic float64) {
	tmpHigh, tmpLow := highLow(values)
	return ((tmpHigh - values[len(values)-1].Close) / (tmpHigh - tmpLow)) * 100.0
} // basicWilliams ()

// CompleteCurrentWilliamsR calculates the complete current Williams%R
func CompleteCurrentWilliamsR(symbol string, period int, dayLevel, weekLevel, monthLevel, yearLevel float64) (evalValues map[string]Evaluate) {
	evalValues = make(map[string]Evaluate)

	quoteVal := postgresql.FindQuotes(symbol, "asc")
	DFS := WilliamsRP(quoteVal, period)

	weekValues := postgresql.WeeklyQuotes(quoteVal)
	WFS := WilliamsRP(weekValues, period)

	monthValues := postgresql.MonthlyQuotes(quoteVal)
	MFS := WilliamsRP(monthValues, period)

	yearValues := postgresql.YearlyQuotes(quoteVal)
	YFS := WilliamsRP(yearValues, period)

	evalValues["D"] = EvaluateValues(DFS.WRValue, dayLevel)
	evalValues["W"] = EvaluateValues(WFS.WRValue, weekLevel)
	evalValues["M"] = EvaluateValues(MFS.WRValue, monthLevel)
	evalValues["Y"] = EvaluateValues(YFS.WRValue, yearLevel)

	return
} // CompleteCurrentWilliamsR ()
