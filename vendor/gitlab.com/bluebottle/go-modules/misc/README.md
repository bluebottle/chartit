Short snippets of functions that are used in different go programs but are too small to justify a modules of its own.

Current files:

| File | Description |
| :--- | :--- |
| **formats** | Some ANSI color settings and other format functions |
| **errors** | A few functions to handle errors |
| **sliceops** | Functions to add or remove from slices |
