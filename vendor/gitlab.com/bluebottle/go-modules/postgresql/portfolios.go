// Package postgresql Some PostgreSQL using functions
package postgresql

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/bluebottle/go-modules/misc"
)

// Portfolio is the structure of a portfolio.
type Portfolio struct {
	PortfolioID uint64  // portfolio_id
	Name        string  // name
	TradeRisk   float64 // trade_risk
	PortRisk    float64 // port_risk
}

var portfolio Portfolio

// FindAllPortfolios returns all portfolios in the portfolio table.
func FindAllPortfolios() (pFolios []Portfolio, err error) {
	var conn *pgxpool.Conn

	if conn, err = pool.Acquire(context.Background()); err != nil {
		return []Portfolio{}, err
	} // if

	if rows, err = conn.Query(context.Background(), "select portfolio_id, name, trade_risk, port_risk from public.portfolio"); err != nil {
		return []Portfolio{}, err
	} // if

	var tmpFolio Portfolio

	for rows.Next() {
		if tmpFolio, err = GetNextPortfolioRow(rows); err != nil {
			return []Portfolio{}, err
		} // if

		pFolios = append(pFolios, tmpFolio)
	} // for

	conn.Release()
	return
} // FindAllPortfolios()

// FindSpecificPortfolio returns portofilo with given name and error status
func FindSpecificPortfolio(name string) (Portfolio, error) {
	var conn *pgxpool.Conn

	if conn, err = pool.Acquire(context.Background()); err != nil {
		return Portfolio{}, err
	} // if

	defer conn.Release()

	if _, err = conn.Conn().Prepare(context.Background(), "findPort", "select portfolio_id, name, trade_risk, port_risk from public.portfolio where name=$1"); err != nil {
		return Portfolio{}, err
	} // if

	if rows, err = conn.Query(context.Background(), "findPort", name); err != nil {
		return Portfolio{}, err
	} // if

	defer rows.Close()

	if rows.Next() {
		return GetNextPortfolioRow(rows)
	} else {
		return Portfolio{}, nil
	} // if
} // FindSpecificPortfolio()

// GetNextPortfolioRow returns the next portfolio row.
func GetNextPortfolioRow(rows pgx.Rows) (Portfolio, error) {
	err := rows.Scan(&portfolio.PortfolioID, &portfolio.Name, &portfolio.TradeRisk, &portfolio.PortRisk)
	return portfolio, err
} // GetNextPortfolioRow ()

// AddEmptyPortfolio will add a new, empty portfolio
func AddEmptyPortfolio(name string) {
	AddPortfolio(name, 0.0, 0.0)
} // AddEmptyPortfolio ()

// AddPortfolio will add a new portfolio with given values
func AddPortfolio(name string, tr, pr float64) {
	tmpPort, err := FindSpecificPortfolio(name)
	misc.ShowError(err, "", "ErrPanic")

	// add new portfolio if no portfolio with that name exists
	if tmpPort.Name != name {
		conn, err = pool.Acquire(context.Background())
		misc.ShowError(err, "", "ErrPanic")
		cmd := fmt.Sprintf("insert into public.portfolio(name, trade_risk, port_risk) values ('%s', %f, %f)", name, tr, pr)
		_, err := conn.Exec(context.Background(), cmd)
		misc.ShowError(err, "", "ErrPanic")
		conn.Release()
	} // if
} // AddPortfolio ()
