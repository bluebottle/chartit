package postgresql

import (
	"context"
	"fmt"

	"gitlab.com/bluebottle/go-modules/misc"
)

// CurrSymbCross contains the structure of a curr_symb_cross row
type CurrSymbCross struct {
	IsoCodeSrc  string
	IsoCodeDest string
	Symbol      string
}

// NewCurrSymbCross returns an empty curr_symb_cross row
func NewCurrSymbCross() CurrSymbCross {
	return CurrSymbCross{
		IsoCodeSrc:  "",
		IsoCodeDest: "",
		Symbol:      "",
	}
} // NewCurrSymbCross ()

// GetCurrencyXSymbol returns symbol for given currency pair.
func GetCurrencyXSymbol(src, dest string) (symbol string) {
	var row CurrSymbCross
	conn, err = pool.Acquire(context.Background())
	misc.ShowError(err, "", "ErrPanic")
	rows, err = conn.Query(context.Background(), fmt.Sprintf("select iso_code_src, iso_code_dest, symbol from public.curr_symb_cross where iso_code_src='%s' and iso_code_dest='%s'", src, dest))
	misc.ShowError(err, "", "ErrPanic")

	if rows.Next() {
		err := rows.Scan(&row.IsoCodeSrc, &row.IsoCodeDest, &row.Symbol)
		misc.ShowError(err, "", "ErrPanic")
		symbol = row.Symbol
	} else {
		symbol = ""
	} // if

	conn.Release()
	return
} // GetCurrencyXSymbol ()
