// Package postgresql Some PostgreSQL using functions
package postgresql

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/bluebottle/go-modules/misc"
)

var (
	// internal database pointer
	pool *pgxpool.Pool = nil
	rows pgx.Rows      = nil
	conn *pgxpool.Conn = nil
	err  error
)

// NewDBConnection Creates the database handler and checks the connection.
// allowed sslmode values are: disable, allow, prefer, require, verify-ca and verify-full
func NewDBConnection(user, host, password, port, dbname, appName, sslMode string) (err error) {
	tmpURL := fmt.Sprintf("user=%s host=%s password=%s port=%s database=%s sslmode=%s application_name=%s", user, host, password, port, dbname, sslMode, appName)
	pool, err = pgxpool.New(context.Background(), tmpURL)
	misc.ShowError(err, "", "ErrFatal")
	_, err = pool.Exec(context.Background(), "SELECT 1")
	misc.ShowError(err, "", "ErrFatal")
	//
	// TODO: Log error on log file instead of returning?
	//
	return
} // NewDBConnection ()

// CloseConnection closes the current connection
func CloseConnection() {
	pool.Close()
} // CloseConnection ()
