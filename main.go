package main

import (
	"gitlab.com/bluebottle/chartit/gui"
)

func main() {
	gui.Create().Run()
} // main ()
