package main

import (
	"fmt"
	"os"
	"sort"
	"strings"
	"time"

	"gitlab.com/bluebottle/chartit/configuration"
	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/bluebottle/go-modules/postgresql"

	"github.com/urfave/cli/v2"

	"golang.org/x/exp/slices"
)

func main() {
	var symbol string
	var number int
	var excludes cli.StringSlice

	app := &cli.App{
		Name:                   "updateloop",
		Usage:                  "Updating securities",
		UseShortOptionHandling: true,
		Version:                "0.1",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "symbol",
				Aliases:     []string{"s"},
				Usage:       "Symbol to update",
				Destination: &symbol,
				Value:       "",
			},
			&cli.IntFlag{
				Name:        "number",
				Aliases:     []string{"n"},
				Usage:       "Number of securities to update before exiting",
				Destination: &number,
				Value:       0,
			},
			&cli.StringSliceFlag{
				Name:        "exclude",
				Aliases:     []string{"e"},
				Usage:       "Isin to exclude (could be given multiple times)",
				Destination: &excludes,
				Value:       &cli.StringSlice{},
			},
		},
		Action: func(c *cli.Context) error {
			mainWork(symbol, number, excludes, "updateloop")
			return nil
		},
	}

	err := app.Run(os.Args)
	misc.ShowError(err, "", "ErrPanic")
} // func main

// Update all securities
func mainWork(symbol string, number int, excludes cli.StringSlice, name string) {
	// Read config file.
	config := configuration.ReadProgConfig("/etc/chartit/chartit.yml")
	// open database connection
	err := postgresql.NewDBConnection(
		config.String("database.user"),
		config.String("database.host"),
		config.String("database.password"),
		config.String("database.port"),
		config.String("database.dbname"),
		name,
		config.String("database.sslmode"))
	misc.ShowError(err, "Could not open postgres database.", "ErrMsgFatal")
	var allLookup []postgresql.Lookup

	// get all lookup values or only one if symbol is set
	if symbol == "" {
		allLookup = sortedLookup(number, excludes.Value())
	} else {
		allLookup = []postgresql.Lookup{postgresql.GetSingleLookup(symbol)}
	} // if

	fmt.Println("Start update quotes: " + time.Now().String())
	postgresql.UpdateQuoteLoop(allLookup)
	postgresql.CloseConnection()
	fmt.Println("End update quotes: " + time.Now().String())
} // mainWork ()

// get all lookup values and sort them by last quote date ascending (we want to update the oldest securities first)
func sortedLookup(number int, excludes []string) (allLookup []postgresql.Lookup) {
	// get all lookup values
	allLookup = postgresql.FindAllCurrentLookup()
	var sortedValues map[string][]postgresql.Lookup = make(map[string][]postgresql.Lookup)

	// get last quote dates for all symbols
	for _, v := range allLookup {
		//
		// TODO: Disable ariva symbols until they are all deleted or changed
		//
		if !strings.HasPrefix(v.Symbol, "ariva::") {
			// Only process included values
			if !slices.Contains(excludes, v.ISIN) {
				tmpQuote := postgresql.GetLastQuoteDate(v.Symbol)
				sortedValues[tmpQuote] = append(sortedValues[tmpQuote], v)
			} // if
		} // if
	} // for

	// create slice to sort
	var tmpSlice []string

	for i := range sortedValues {
		tmpSlice = append(tmpSlice, i)
	} // for

	sort.Strings(tmpSlice)
	// build sorted slice of lookups
	allLookup = allLookup[:0]

	// build slice of complete values
	for _, v := range tmpSlice {
		allLookup = append(allLookup, sortedValues[v]...)
	} // for

	//
	// TODO: Handle empty slices?
	//
	// trim result to desired number if number is set
	if number > 0 {
		allLookup = allLookup[:number]
	} // if

	return
} // sortedLookup ()
