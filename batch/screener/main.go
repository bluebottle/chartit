package main

import (
	"fmt"
	"math"
	"os"

	"github.com/knadh/koanf/v2"
	"github.com/urfave/cli/v2"

	"gitlab.com/bluebottle/chartit/configuration"
	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/bluebottle/go-modules/postgresql"
	"gitlab.com/bluebottle/go-modules/technical"
)

func main() {
	var (
		file string
	)
	app := &cli.App{
		Name:                   "screener",
		Usage:                  "Scanning for investable equities",
		UseShortOptionHandling: true,
		Version:                "0.2",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "file",
				Aliases:     []string{"f"},
				Usage:       "batch input file (default: screener.yml)",
				Destination: &file,
				Value:       "screener.yml",
			},
		},
		Action: func(c *cli.Context) error {
			mainWork("screener", file)

			return nil
		},
	}

	err := app.Run(os.Args)
	misc.ShowError(err, "", "ErrFatal")
} // func main

// func getEMAresults get latest values for set of EMAs and difference of price to chosen EMA value
func getEMAresults(quotes []postgresql.Quote, period string, ema int) (tEMA float64, CEs map[int]float64) {
	var (
		EMAs map[int][]float64 = make(map[int][]float64)
	)
	CEs = make(map[int]float64)

	// calculate EMAs and last EMA value
	for _, v := range []int{10, 20, 40, 50, 100, 150, 200} {
		switch period {
		case "d":
			EMAs[v] = technical.EMA(v, postgresql.GetClose(quotes))
		case "w":
			EMAs[v] = technical.EMA(v, postgresql.GetClose(postgresql.WeeklyQuotes(quotes)))
		case "m":
			EMAs[v] = technical.EMA(v, postgresql.GetClose(postgresql.MonthlyQuotes(quotes)))
		case "y":
			EMAs[v] = technical.EMA(v, postgresql.GetClose(postgresql.YearlyQuotes(quotes)))
		} // switch

		if len(EMAs[v]) > 0 {
			CEs[v] = EMAs[v][len(EMAs[v])-1]
		} else {
			CEs[v] = -1
		} // if
	} // for

	tEMA = technical.EmaDiffPer(EMAs[ema], quotes)
	return
} // getEMAresults ()

// func emaCrossOver checks for given ema cross overs and return symbols, where the shorter ema is above the longer one
func emaCrossOver(lookupVal []postgresql.Lookup, period string, ema1, ema2 int) (tmpLooks []postgresql.Lookup) {
	fmt.Printf("Checking EMA%d and EMA%d crossing (period %s) (%d securites)\n", ema1, ema2, period, len(lookupVal))
	var (
		emaLow, emaHigh int
		closeValues     []float64
	)

	// check for the lower ema
	if ema1 < ema2 {
		emaLow = ema1
		emaHigh = ema2
	} else {
		emaLow = ema2
		emaHigh = ema1
	} // if

	for _, values := range lookupVal {
		// get quotes for security and given period
		tmpQuotes := postgresql.FindQuotes(values.Symbol, "asc")

		switch period {
		case "d":
			closeValues = postgresql.GetClose(tmpQuotes)
		case "w":
			closeValues = postgresql.GetClose(postgresql.WeeklyQuotes(tmpQuotes))
		case "m":
			closeValues = postgresql.GetClose(postgresql.MonthlyQuotes(tmpQuotes))
		case "y":
			closeValues = postgresql.GetClose(postgresql.YearlyQuotes(tmpQuotes))
		} // switch

		// get values of both EMAs
		emaLowRes := technical.EMA(emaLow, closeValues)
		emaHighRes := technical.EMA(emaHigh, closeValues)

		if emaLowRes[len(emaLowRes)-1] > emaHighRes[len(emaHighRes)-1] {
			tmpLooks = append(tmpLooks, values)
		} // if
	} // for

	// keep only passing securites for next check
	return
} // emaCrossOver ()

func emaCalc(lookupVal []postgresql.Lookup, emaPeriod string, emadiff float64, ema int) (tmpLooks []postgresql.Lookup) {
	fmt.Printf("Checking if EMAs are stacking up and price is max %4.2f%% above EMA%d(%s) (%d securites)\n", emadiff, ema, emaPeriod, len(lookupVal))

	// TODO: Optimize function (gocognit 16)
	for _, values := range lookupVal {
		QuoteVal := postgresql.FindQuotes(values.Symbol, "asc")
		tmpEMA, CEs := getEMAresults(QuoteVal, emaPeriod, ema)

		// Keep only those securities whose EMAs are stacked in the right order and
		// whose price is currently only a certain percentage above chosen EMA
		if CEs[10] > CEs[20] && CEs[20] > CEs[40] && CEs[40] > CEs[50] && CEs[50] > CEs[100] &&
			CEs[100] > CEs[150] && CEs[150] > CEs[200] && tmpEMA <= emadiff && tmpEMA >= 0 {
			tmpLooks = append(tmpLooks, values)
		} // if
	} // for

	// passing securites for next check
	return
} // emaCalc ()

func stochCalc(lookupVal []postgresql.Lookup, stochPeriod, k, d int, daily, weekly, monthly, yearly float64) (tmpLooks []postgresql.Lookup) {
	fmt.Printf("Checking Stochastics(%d %d %d) are below (d%2.2f/w%2.2f/m%2.2f/y%2.2f) (%d securites)\n", stochPeriod, k, d, daily, weekly, monthly, yearly, len(lookupVal))
	var evalValues map[string]technical.Evaluate

	for _, values := range lookupVal {
		evalValues = technical.CompleteCurrentStochastic(values.Symbol, stochPeriod, k, d, daily, weekly, monthly, yearly)

		if evalValues["D"].IsLower && evalValues["W"].IsLower &&
			(evalValues["M"].IsLower || evalValues["M"].CurrentValue == -1.0) &&
			(evalValues["Y"].IsLower || evalValues["Y"].CurrentValue == -1.0) {
			tmpLooks = append(tmpLooks, values)
		} // if
	} // for

	// keep only passing securites for next check
	return
} // stochCalc()

func willCalc(lookupVal []postgresql.Lookup, willPeriod int, wdaily, wweekly, wmonthly, wyearly float64) (tmpLooks []postgresql.Lookup) {
	fmt.Printf("Checking Williams%%R(%d) is below (d%2.2f/w%2.2f/m%2.2f/y%2.2f) (%d securites)\n", willPeriod, wdaily, wweekly, wmonthly, wyearly, len(lookupVal))
	var evalValues map[string]technical.Evaluate

	for _, values := range lookupVal {
		evalValues = technical.CompleteCurrentWilliamsR(values.Symbol, willPeriod, wdaily, wweekly, wmonthly, wyearly)

		if evalValues["D"].IsLower && evalValues["W"].IsLower &&
			(evalValues["M"].IsLower || evalValues["M"].CurrentValue == -1.0) &&
			(evalValues["Y"].IsLower || evalValues["Y"].CurrentValue == -1.0) {
			tmpLooks = append(tmpLooks, values)
		} // if
	} // for

	// keep only passing securites for next check
	return
} // willCalc ()

func macdCalc(lookupVal []postgresql.Lookup, emaPeriod string, emaLow, emaHigh, emaSignal int) (tmpLooks []postgresql.Lookup) {
	fmt.Printf("Checking if MACD(%s,%d,%d,%d) is positive (%d securites)\n", emaPeriod, emaLow, emaHigh, emaSignal, len(lookupVal))

	//
	// TODO: Add check for just switch positive?
	//
	for _, values := range lookupVal {
		ma, signal := technical.MACD(lookupVal, values.Symbol, emaPeriod, emaLow, emaHigh, emaSignal)

		// Keep only those securities whose MACD is positive
		if (ma[len(ma)-1] - signal[len(signal)-1]) > 0 {
			tmpLooks = append(tmpLooks, values)
		} // if
	} // for

	// passing securites for next check
	return
} // macdCalc ()

func mainLoop(lookupVal []postgresql.Lookup, batch *koanf.Koanf) (retLook []postgresql.Lookup) {
	// Initialize array of lookup values
	retLook = lookupVal

	for _, k := range batch.Slices("tasks") {
		switch {
		case k.Exists("EMAX"):
			retLook = emaCrossOver(
				retLook,
				k.String("EMAX.emaPeriod"),
				k.Int("EMAX.ema1"),
				k.Int("EMAX.ema2"),
			)
		case k.Exists("EMA"):
			retLook = emaCalc(
				retLook,
				k.String("EMA.emaPeriod"),
				k.Float64("EMA.emadiff"),
				k.Int("EMA.ema"),
			)
		case k.Exists("MACD"):
			retLook = macdCalc(
				retLook,
				k.String("MACD.emaPeriod"),
				k.Int("MACD.emaLow"),
				k.Int("MACD.emaHigh"),
				k.Int("MACD.emaSignal"),
			)
		case k.Exists("Stochastic"):
			retLook = stochCalc(
				retLook,
				k.Int("Stochastic.stochPeriod"),
				k.Int("Stochastic.stochK"),
				k.Int("Stochastic.stochD"),
				k.Float64("Stochastic.daily"),
				k.Float64("Stochastic.weekly"),
				k.Float64("Stochastic.monthly"),
				k.Float64("Stochastic.yearly"),
			)
		case k.Exists("Williams%R"):
			retLook = willCalc(
				retLook,
				k.Int("Williams%R.willPeriod"),
				k.Float64("Williams%R.wdaily"),
				k.Float64("Williams%R.wweekly"),
				k.Float64("Williams%R.wmonthly"),
				k.Float64("Williams%R.wyearly"),
			)
		case k.Exists("RRG"):
			retLook = rrgCalc(
				retLook,
				k.String("RRG.status"),
				k.String("RRG.benchmark"),
				k.String("RRG.period"),
				k.Int("RRG.length"),
			)
		default:
			fmt.Println("Found wrong element")
		} // switch
	} // for

	return
} // mainLoop

// Scans all equities depending on the checks and values chosen
func mainWork(appname string, filename string) {
	// Read config files
	//
	// TODO: Minimize the use of hardcoded values
	//
	config := configuration.ReadProgConfig("/etc/chartit/chartit.yml")
	batch := configuration.ReadBatchConfig(filename)
	// open database connection
	err := postgresql.NewDBConnection(
		config.String("database.user"),
		config.String("database.host"),
		config.String("database.password"),
		config.String("database.port"),
		config.String("database.dbname"),
		appname,
		config.String("database.sslmode"))
	misc.ShowError(err, "Could not open postgres database.", "ErrMsgFatal")

	var (
		lookupVal []postgresql.Lookup
	)

	// Get lookup values if any tasks are given
	if batch.Exists("tasks") && batch.MustString("tasks") != "" {
		lookupVal = postgresql.FindAllCurrentLookup()
		// Print header
		fmt.Printf("Scanning %d securities\n", len(lookupVal))
	} else {
		fmt.Println("No tasks given")
	} // if

	// First run through the loop
	lookupVal = mainLoop(lookupVal, batch)
	// output passing securities
	fmt.Printf("%d securities passed screening\n", len(lookupVal))
	fmt.Println("Updating resulting values")
	// Update all resulting values, to make sure we are scanning the latest values
	postgresql.UpdateQuoteLoop(lookupVal)
	// Final run through the loop
	lookupVal = mainLoop(lookupVal, batch)
	// output passing securities
	fmt.Printf("%d securities passed screening\n", len(lookupVal))

	if len(lookupVal) > 0 {
		fmt.Println("\nName (ISIN/Symbol)")

		for _, v := range lookupVal {
			fmt.Printf("%s (%s/%s)\n", v.Name, v.ISIN, v.Symbol)
		} // for
	} // if

	postgresql.CloseConnection()
} // mainWork ()

// Calculate relative strength of given quotes
func RelativeStrength(security, benchmark []postgresql.Quote, length int) (RSResult []float64) {
	var (
		benchMap    map[string]postgresql.Quote = make(map[string]postgresql.Quote)
		benchLength int                         = len(benchmark)
	)

	// initialize return value
	RSResult = append(RSResult, 0)

	// create bench map for comparison
	for _, values := range benchmark {
		benchMap[values.QuoteDate.String()] = values
	} // for

	// Go through all stock values
	for i, values := range security {
		// Check if a corresponding benchmark value (date) exists
		if benchValue, ok := benchMap[values.QuoteDate.String()]; ok {
			// Add calculated strength to the result
			if i > length && i < len(security) && i < benchLength {
				//				RSResult = append(RSResult, 100*(values.Close/security[i-length].Close/(benchValue.Close/benchmark[i-length].Close)))
				//RSResult = append(RSResult, 100*((values.Close- security[i-length].Close)/(benchValue.Close-benchmark[i-length].Close)))
				//				RSResult = append(RSResult, 100*(values.Close/benchValue.Close))
				RSResult = append(RSResult, 100*(values.Close/benchValue.Close))
			} else {
				RSResult = append(RSResult, 0)
			} // if
		} // if
	} // for

	return
} // RelativeStrength ()

// standard deviation
func StandardDeviation(num []float64, period int) (sd float64) {
	var sum, mean float64
	length := len(num)

	for i := 0; i < period; i++ {
		//		for i, _ := range num {
		sum += num[length-i-1]
	} // for

	mean = sum / float64(period)

	for i := 0; i < period; i++ {
		//		for i, _ := range num {
		// The use of Pow math function func Pow(x, y float64) float64
		sd += math.Pow(num[length-i]-mean, 2)
	} // for

	// The use of Sqrt math function func Sqrt(x float64) float64
	return math.Sqrt(sd / float64(period))
} // StandardDeviation

func rrgCalc(lookupVal []postgresql.Lookup, status, benchmark, RRGPeriod string, length int) (tmpLooks []postgresql.Lookup) {
	fmt.Printf("Checking RRG status(%s) (%s), benchmark (%s) (%d securites)\n", RRGPeriod, status, benchmark, len(lookupVal))

	// get benchmark quotes
	//	QuoteValBench := postgresql.GetPeriodQuotes(benchmark, RRGPeriod)

	//	for _, values := range lookupVal {
	// get security quotes
	//		QuoteVal := postgresql.GetPeriodQuotes(values.Symbol, RRGPeriod)
	// Calculate relative strength
	// rs_tickers.append(100 * (tickers_data[tickers[i]]/ benchmark_data))
	//		relativeStrength := RelativeStrength(QuoteVal, QuoteValBench, length)
	//		SmaArr := technical.SMA(length, relativeStrength)
	//		SDArr := StandardDeviation(relativeStrength, length)
	// Calculate relative strength ratio
	// rsr_tickers.append((100 + (rs_tickers[i] - rs_tickers[i].rolling(window=window).mean()) / rs_tickers[i].rolling(window=window).std(ddof=0)).dropna())
	//	relativeStrengthRatio := (100 + (relativeStrength[]))
	// rsr_roc_tickers.append(100 * ((rsr_tickers[i]/ rsr_tickers[i][1]) - 1))
	// # Relative strength momentum
	// rsm_tickers.append((101 + ((rsr_roc_tickers[i] - rsr_roc_tickers[i].rolling(window=window).mean()) / rsr_roc_tickers[i].rolling(window=window).std(ddof=0))).dropna())
	// rsr_tickers[i] = rsr_tickers[i][rsr_tickers[i].index.isin(rsm_tickers[i].index)]
	// rsm_tickers[i] = rsm_tickers[i][rsm_tickers[i].index.isin(rsr_tickers[i].index)]
	//		fmt.Printf("%s %5.2f\n", values.Name, relativeStrength)
	//	} // for

	// keep only passing securites for next check
	return
} // rrgCalc()

// for i in range(len(tickers)):
//     # Relative strength
//     rs_tickers.append(100 * (tickers_data[tickers[i]]/ benchmark_data))
//     # Relative strength ratio
//     rsr_tickers.append((100 + (rs_tickers[i] - rs_tickers[i].rolling(window=window).mean()) / rs_tickers[i].rolling(window=window).std(ddof=0)).dropna())
//     rsr_roc_tickers.append(100 * ((rsr_tickers[i]/ rsr_tickers[i][1]) - 1))
//     # Relative strength momentum
//     rsm_tickers.append((101 + ((rsr_roc_tickers[i] - rsr_roc_tickers[i].rolling(window=window).mean()) / rsr_roc_tickers[i].rolling(window=window).std(ddof=0))).dropna())
//     rsr_tickers[i] = rsr_tickers[i][rsr_tickers[i].index.isin(rsm_tickers[i].index)]
//     rsm_tickers[i] = rsm_tickers[i][rsm_tickers[i].index.isin(rsr_tickers[i].index)]

// //stdDev := standardDeviation(relativeStrength, period)
// // rsr = RS-Ratio
// // rsr = 101 + ((rs - sma(rs, period)) / stdev(rs, period))
// //relativeStrengthRatio := 101 + (relativeStrength - technical.SMA(period, QuoteVal))/stdDev
// // with relative momentum?
// // momoRS = ((baseSymbol / ta.sma(baseSymbol, MoMo_length)) / (comparativeSymbol / ta.sma(comparativeSymbol, MoMo_length)) - 1.0) * 100
// relativeStrengthRatio := 100 - (100 / (1+relativeStrength[len(relativeStrength)-1]))
// fmt.Printf("%s %5.2f\n", values.Name, relativeStrengthRatio)
// //	rsrRoc = 100 * ((rsr / rsr[1]) - 1)
// //
// // rsm = RS-Momentum
// //	rsm = 101 + ((rsrRoc - sma(rsrRoc, period)) / stdev(rsrRoc, period))

// //	status = "null"
// //	bgcolor = color.white
// //	if rsr < 100 and rsm > 100
// //	    status := "improving"
// //	else if rsr > 100 and rsm > 100
// //	    status := "leading"
// //	else if rsr > 100 and rsm < 100
// //	    status := "weakening"
// //	else if rsr < 100 and rsm < 100
// //	    status := "lagging"
