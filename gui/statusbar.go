package gui

import (
	"github.com/g3n/engine/gui"
)

// Handles the creation and actions of the status bar
type statBar struct {
	pan  *gui.Panel
	text *gui.Label
}

// Set up status bar
func (st *statBar) addStatusbar() {
	st.pan = gui.NewPanel(0, 0)
	st.pan.SetLayoutParams(&gui.DockLayoutParams{Edge: gui.DockBottom})
	st.pan.SetBordersColor4(&guiBrown)
	st.pan.SetBorders(1, 0, 0, 0)
	statLayout := gui.NewHBoxLayout()
	statLayout.SetAutoHeight(true)
	st.pan.SetLayout(statLayout)
	st.text = gui.NewLabel("")
	st.pan.Add(st.text)
} // addStatusbar ()
