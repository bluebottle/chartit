package gui

import (
	"fmt"

	"github.com/g3n/engine/gui"
	"github.com/g3n/engine/window"
	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/bluebottle/go-modules/postgresql"
)

type portfolioTab struct {
	panel        *gui.Panel // Main panel of the tab
	topPanel     *gui.Panel
	tab          *gui.Tab
	topDropdown  *gui.DropDown  // dropdown menu with portfolio names
	portInfoFold portInfoFolder // Folder for the info of the selected portfolio
	horSplitter  *gui.Splitter
}

func (pT *portfolioTab) addPortfolioTab() {
	pT.tab.SetPinned(true)
	// Adding main panel
	pT.panel = gui.NewPanel(0, 0)
	// Adding top panel
	pT.topPanel = gui.NewPanel(0, 0)
	tpLayout := gui.NewHBoxLayout()
	tpLayout.SetAlignH(gui.AlignLeft)
	pT.topPanel.SetLayout(tpLayout)
	pT.topPanel.SetPosition(0, 0)
	pT.topPanel.SetHeight(25)
	pT.topPanel.Add(gui.NewLabel("Portfolio: "))
	// Add dropdown menu with existing portfolios
	pT.topDropdown = gui.NewDropDown(150, gui.NewImageLabel(""))
	// read database values and add them to the form
	prefillPortfolio(pT.topDropdown)
	pT.topPanel.Add(pT.topDropdown)
	pT.panel.Add(pT.topPanel)

	pT.horSplitter = gui.NewHSplitter(0, 0)
	pT.horSplitter.SetPosition(0, 25)
	pT.horSplitter.SetBordersColor(&guiBlack)
	pT.horSplitter.SetBorders(1, 1, 1, 1)
	pT.horSplitter.SetSplit(0.25)
	// Add left side of the splitter
	pT.portInfoFold.addInfoFolder()
	pT.horSplitter.P0.Add(pT.portInfoFold.folder)

	// Create table
	//
	// TODO: Fix column sorting (float sortable?), somehow numbers do not work, string sort case-insensitive? Or are the mixed values to blame?
	// TODO: Adding colors to table cells?
	// TODO: Move some of the values somewhere else?
	// TODO: Add values like difference to EMA into this view as well?
	// TODO: Automatically select first line, if exists?
	// TODO: Add menu (either on right click or as button) to update or alter selected values
	//
	tmpTab := buildTable([]gui.TableColumn{
		{Id: "1", Header: "Name", Width: 250, Minwidth: 250, Align: gui.AlignRight, Sort: gui.TableSortString, Format: "%s", Expand: 0, Resize: true},
		{Id: "2", Header: "ISIN", Width: 250, Minwidth: 200, Align: gui.AlignRight, Sort: gui.TableSortString, Format: "%s", Expand: 0, Resize: true},
		{Id: "3", Header: "Buy Date", Width: 100, Minwidth: 100, Align: gui.AlignRight, Sort: gui.TableSortString, Format: "%s", Expand: 0, Resize: true},
		{Id: "4", Header: "Buy Quantity", Width: 50, Minwidth: 50, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.2f", Expand: 0, Resize: true},
		{Id: "5", Header: "Buy Price", Width: 50, Minwidth: 50, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.2f", Expand: 0, Resize: true},
		//		{Id: "6", Header: "Buy\nBrokerage", Width: 50, Minwidth: 50, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.2f", Expand: 0, Resize: true},
		//		{Id: "7", Header: "Buy\nSlippage", Width: 50, Minwidth: 50, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.2f", Expand: 0, Resize: true},
		//		{Id: "8", Header: "Buy 1R", Width: 50, Minwidth: 50, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.2f", Expand: 0, Resize: true},
		//		{Id: "9", Header: "Buy\nTotal", Width: 75, Minwidth: 75, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.3f", Expand: 0, Resize: true},
		//
		// TODO: Separate numbers from textual output to allow sorting
		//
		{Id: "10", Header: "Price", Width: 100, Minwidth: 100, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%s", Expand: 0, Resize: true},
		{Id: "11", Header: "Stop Loss Type", Width: 50, Minwidth: 50, Align: gui.AlignRight, Sort: gui.TableSortString, Format: "%s", Expand: 0, Resize: true},
		//		{Id: "12", Header: "Diff to\nStop Loss", Width: 50, Minwidth: 50, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%s", Expand: 0, Resize: true},
		{Id: "13", Header: "Total €", Width: 75, Minwidth: 75, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.3f", Expand: 0, Resize: true},
		{Id: "14", Header: "Profit/Loss €", Width: 100, Minwidth: 100, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.3f", Expand: 0, Resize: true},
		{Id: "15", Header: "Profit/Loss %", Width: 50, Minwidth: 50, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.3f", Expand: 0, Resize: true},
		//		{Id: "16", Header: "1R", Width: 50, Minwidth: 50, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.2f", Expand: 0, Resize: true},
		//		{Id: "17", Header: "Multiple R", Width: 100, Minwidth: 100, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.2f", Expand: 0, Resize: true},
		{Id: "18", Header: "Days", Width: 50, Minwidth: 50, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%d", Expand: 0, Resize: true},
		//{Id: "19", Header: "% Portfolio", Width: 50, Minwidth: 50, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.2f", Expand: 0, Resize: true},
		//{Id: "20", Header: "Risk/Trade", Width: 100, Minwidth: 100, Align: gui.AlignRight, Sort: gui.TableSortNumber, Format: "%3.2f", Expand: 0, Resize: true},
	})

	// Fill table with values from currently selected portfolio
	tmpTab.SetRows(fillPortfolio(pT.topDropdown.Selected().Text()))
	// Allow multiple rows to be selected
	tmpTab.SetSelectionType(gui.TableSelMultiRow)

	// Add table row menu
	rowMenu := gui.NewMenu()
	rowMenu.AddOption("Update selected values").SetId("updateValues")
	rowMenu.SetVisible(false)
	rowMenu.SetBounded(false)
	rowMenu.Subscribe(gui.OnMouseDownOut, func(evname string, ev interface{}) {
		rowMenu.SetVisible(false)
	})
	tmpTab.Add(rowMenu)

	var tce gui.TableClickEvent
	tmpTab.Subscribe(gui.OnTableClick, func(evname string, ev interface{}) {
		//tce = ev.(gui.TableClickEvent)

		if tce.Button != window.MouseButtonRight {
			return
		} // if

		//
		// TODO: Why does the menu not appear on top?
		//
		if tce.Row >= 0 {
			rowMenu.SetPosition(tce.X, tce.Y)
			rowMenu.SetVisible(true)
			return
		} // if
	})

	// Add actions for row menu entries
	rowMenu.Subscribe(gui.OnClick, func(evname string, ev interface{}) {
		rowMenu.SetVisible(false)
		opid := ev.(*gui.MenuItem).Id()

		switch opid {
		case "updateValues":
			fmt.Println("Update me !!!!!")
		}
	})
	//
	// TODO: Add header and fields like PF?
	//
	pT.horSplitter.P1.Add(tmpTab)
	pT.panel.Add(pT.horSplitter)

	// Resizes table when window resizes
	pT.panel.SubscribeID(gui.OnResize, pT, func(evname string, ev interface{}) {
		tmpTab.SetSize(pT.panel.ContentWidth(), pT.panel.ContentHeight())
	})

	pT.topDropdown.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		tmpTab.Clear()
		tmpTab.SetRows(fillPortfolio(pT.topDropdown.Selected().Text()))
	})

	pT.tab.SetContent(pT.panel)
} // addPortfolioTab ()

// Read all trade values from database and return values for selected portfolio
func fillPortfolio(portName string) (portFL []map[string]interface{}) {
	tmpPortNum, err := postgresql.FindSpecificPortfolio(portName)
	misc.ShowError(err, "", "ErrPrint")
	tmpTrades, err := postgresql.FindPortfolioTrades(tmpPortNum.PortfolioID)
	misc.ShowError(err, "", "ErrPrint")
	portFL = make([]map[string]interface{}, 0, len(tmpTrades))

	for _, v := range tmpTrades {
		rval := make(map[string]interface{})
		// Use looked up name for symbol
		rval["1"] = postgresql.GetSingleLookup(v.Symbol).Name
		rval["2"] = postgresql.GetSingleLookup(v.Symbol).ISIN
		//
		// TODO: Get buy date, quantity, price, brokerage, slippage... from transactions
		// postgresql.FindPortTradeTransactions (v.PortfolioID, v.TradeID)
		// sort the result before or after?
		// show all transactions or just one?
		//
		//rval["3"] = v.BuyDate.Format("2006-01-02")
		//rval["4"] = v.BuyQuantity
		//rval["5"] = v.BuyPrice
		// //			rval["6"] = v.BuyBrokerage
		// //			rval["7"] = v.BuySlippage
		// //			rval["8"] = v.OneR
		// // Calculate buy total
		// tmpBuyTot := v.BuyPrice*v.BuyQuantity + v.BuyBrokerage + v.BuySlippage
		// rval["9"] = tmpBuyTot
		// Add last quote date to current price
		tmpPrice := postgresql.GetLastQuote(v.Symbol)
		//
		// TODO: We need to separate price from quote date? Maybe in a popup or extra column?
		//
//		rval["10"] = fmt.Sprintf("%3.3f\n(%s)", tmpPrice.Close, tmpPrice.QuoteDate.Format("2006-01-02"))
		rval["10"] = fmt.Sprintf("%3.3f", tmpPrice.Close)
		// Add stop loss type and current value
		//
		// TODO: We need to separate stop loss type from value? Maybe in a popup or extra column?
		//
//		rval["11"] = fmt.Sprintf("%s\n(%3.2f)", v.StopLossType, v.StopLossValue)
		rval["11"] = v.StopLossType
		// Add current diff to stop loss
		// var tmpDiff float64

		// if v.StopLossType == "None" {
		// 	tmpDiff = 0.0
		// } else {
		// 	tmpDiff = (tmpPrice.Close - v.StopLossValue) * 100.0 / v.StopLossValue
		// } // if

		//			rval["12"] = fmt.Sprintf("%3.3f %%", tmpDiff)
		// Add current total
		// rval["13"] = tmpPrice.Close * v.BuyQuantity
		// // Add current profit/loss value
		// tmpWin := (tmpPrice.Close - v.BuyPrice) * v.BuyQuantity
		// rval["14"] = tmpWin
		// //
		// // TODO: Add current profit/loss (percent)
		// //
		// rval["15"] = tmpWin * 100.0 / tmpBuyTot
		// // Add current risk
		// //			rval["16"] = technical.CalculateCurrentRisk(v)
		// // Add multiple one R
		// // if v.OneR > 0 {
		// // 	rval["17"] = tmpWin / v.OneR
		// // } else {
		// // 	rval["17"] = 0.0
		// // } // if
		// // Add days holding
		// rval["18"] = int(time.Since(v.BuyDate).Hours() / 24)
		//
		// TODO: Add percent of portfolio later, when we know the current value of open positions
		//
		//rval["19"] = <PerPort>
		//
		// TODO: Add risk of trade later, when we know the current value of open positions
		//
		//rval["20"] = "RiskTrade"
		portFL = append(portFL, rval)
	} // for

	return
} // fillPortfolio()
