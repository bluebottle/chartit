package gui

import (
	"strconv"

	"github.com/g3n/engine/gui"
	"github.com/g3n/engine/math32"
)

// Helper functions and vars for gui

var (
	guiWhite = math32.Color{R: 1, G: 1, B: 1}
	guiRed   = math32.Color{R: 1, G: 0, B: 0}
	guiBlack = math32.Color{R: 0, G: 0, B: 0}
	guiBrown = math32.Color4{R: 0.71, G: 0.482, B: 0.26, A: 1}
)

// Return a label with colspan for grid layout set
func setGridRadio(str string, col int) (tmp *gui.CheckRadio) {
	tmp = gui.NewRadioButton(str)
	tmp.SetLayoutParams(&gui.GridLayoutParams{ColSpan: col})
	tmp.SetColor(&guiWhite)
	return
} // setGridRadio ()

// Return a label with colspan for grid layout set
func setGridLabel(str string, col int) (tmp *gui.Label) {
	tmp = gui.NewLabel(str)
	tmp.SetLayoutParams(&gui.GridLayoutParams{ColSpan: col})
	tmp.SetColor(&guiWhite)
	return
} // setGridLabel ()

// Return a label with given color
func setLabel(str string, color math32.Color) (tmp *gui.Label) {
	tmp = gui.NewLabel(str)
	tmp.SetColor(&color)
	return
} // setLabel ()

// Return an edit with colspan for grid layout
func setGridEdit(col int) (tmp *gui.Edit) {
	tmp = gui.NewEdit(150, "")
	tmp.SetLayoutParams(&gui.GridLayoutParams{ColSpan: col})
	return
} // setGridEdit ()

// Return float value of edit field
func getEditFloat64(field *gui.Edit) (tmpVal float64) {
	var err error

	if tmpVal, err = strconv.ParseFloat(field.Text(), 64); err != nil {
		tmpVal = 0.0
	} // if

	return
} // getEditFloat64 ()

// Return float value of label
func getLabelFloat64(field *gui.Label) (tmpVal float64) {
	var err error

	if tmpVal, err = strconv.ParseFloat(field.Text(), 64); err != nil {
		tmpVal = 0.0
	} // if

	return
} // getLabelFloat64 ()

// Return an drop down menu with filled positions
func setDropDown(label string, values []string, col int) (tmp *gui.DropDown) {
	tmpLabel := gui.NewImageLabel(label)
	tmpLength := tmpLabel.Width()
	tmp = gui.NewDropDown(tmpLength, tmpLabel)
	tmp.SetLayoutParams(&gui.GridLayoutParams{ColSpan: col})

	//
	// TODO: load values from database and insert into drop down
	//
	for _, v := range values {
		vLabel := gui.NewImageLabel(v)
		tmp.Add(vLabel)

		if vLabel.Width() > tmpLength {
			tmpLength = vLabel.Width()
		} // if
	} // for

	tmp.SetWidth(tmpLength * 1.5)
	return
} // setGridEdit ()
