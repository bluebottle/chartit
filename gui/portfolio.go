package gui

import (
	"fmt"

	"github.com/g3n/engine/gui"

	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/bluebottle/go-modules/postgresql"
)

// Handles the creation and actions of the portfolio window
// Elements of the portfolio window
type portValue struct {
	win       *gui.Window   // window
	dropdown  *gui.DropDown // dropdown menu with portfolio names
	portID    *gui.Label    // current portfolio ID
	tradeRisk *gui.Edit     // risk per trade
	portRisk  *gui.Edit     // risk per portfolio
}

// Set up portfolio window
func (form *portValue) addPortfolio() {
	form.win = gui.NewWindow(500, 410)
	form.win.SetTitle("Portfolios")
	ppLayout := gui.NewVBoxLayout()
	ppLayout.SetAutoHeight(true)
	ppLayout.SetAutoWidth(true)
	ppLayout.SetSpacing(2)
	form.win.SetLayoutParams(ppLayout)
	form.win.SetResizable(false)

	// Create panel and set it to window size
	portPan := gui.NewPanel(0, 0)
	portPan.SetColor(&guiBlack)
	portPan.SetLayout(ppLayout)
	form.win.Subscribe(gui.OnResize, func(evname string, ev interface{}) {
		portPan.SetPositionX(0)
		portPan.SetPositionY(25)
		portPan.SetSize(form.win.ContentWidth(), portPan.ContentHeight())
	})

	// Create form elements and add them
	// Define some layouts first
	// aligned left layout
	prLayoutL := gui.NewHBoxLayout()
	prLayoutL.SetAutoHeight(true)
	prLayoutL.SetAlignH(gui.AlignLeft)
	// aligned center layout
	prLayoutC := gui.NewHBoxLayout()
	prLayoutC.SetAutoHeight(true)
	prLayoutC.SetAlignH(gui.AlignCenter)

	// First line
	portFL := gui.NewPanel(0, 0)
	portFL.SetLayout(prLayoutL)
	portFL.Add(setLabel("Portfolio name", guiWhite))
	// Dropdown
	form.dropdown = gui.NewDropDown(100, gui.NewImageLabel(""))
	form.dropdown.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		form.showPortfolioValues()
		form.win.Dispatch(gui.OnResize, nil)
	})
	portFL.Add(form.dropdown)
	// Spacer
	pSp := gui.NewPanel(0, 0)
	params := gui.HBoxLayoutParams{Expand: 1}
	pSp.SetLayoutParams(&params)
	portFL.Add(pSp)
	portFL.Add(setLabel("Portfolio ID: ", guiWhite))
	form.portID = setLabel("X", guiWhite)
	portFL.Add(form.portID)
	portPan.Add(portFL)

	// // Second line
	// portSL := gui.NewPanel(0, 0)
	// portSL.SetLayout(prLayoutC)
	// portSL.Add(setLabel("Capital", guiRed))
	// portPan.Add(portSL)

	// // Third line
	// portTL := gui.NewPanel(0, 0)
	// portTL.SetLayout(prLayoutL)
	// portTL.Add(setLabel("Initial", guiWhite))
	// // Edit
	// form.initCapital = gui.NewEdit(150, "")
	// portTL.Add(form.initCapital)
	// // Spacer
	// ptSp := gui.NewPanel(0, 0)
	// ptSp.SetLayoutParams(&params)
	// portTL.Add(ptSp)
	// portTL.Add(setLabel("Additional", guiWhite))
	// // Edit
	// form.addCapital = gui.NewEdit(150, "")
	// portTL.Add(form.addCapital)
	// portPan.Add(portTL)

	// // Fourth line
	// portFOL := gui.NewPanel(0, 0)
	// portFOL.SetLayout(prLayoutL)
	// portFOL.Add(setLabel("Subtracted", guiWhite))
	// // Edit
	// form.subCapital = gui.NewEdit(150, "")
	// portFOL.Add(form.subCapital)
	// // Spacer
	// pfSp := gui.NewPanel(0, 0)
	// pfSp.SetLayoutParams(&params)
	// portFOL.Add(pfSp)
	// portFOL.Add(setLabel("Current", guiWhite))
	// // Edit
	// form.currCapital = gui.NewEdit(150, "")
	// portFOL.Add(form.currCapital)
	// portPan.Add(portFOL)

	// Fifth line
	portFIL := gui.NewPanel(0, 0)
	portFIL.SetLayout(prLayoutC)
	portFIL.Add(setLabel("Risk %", guiRed))
	portPan.Add(portFIL)

	// Six line
	portSIL := gui.NewPanel(0, 0)
	portSIL.SetLayout(prLayoutL)
	portSIL.Add(setLabel("Trade", guiWhite))
	// Edit
	form.tradeRisk = gui.NewEdit(150, "")
	portSIL.Add(form.tradeRisk)
	// Spacer
	psSp := gui.NewPanel(0, 0)
	psSp.SetLayoutParams(&params)
	portSIL.Add(psSp)
	portSIL.Add(setLabel("Portfolio", guiWhite))
	// Edit
	form.portRisk = gui.NewEdit(150, "")
	portSIL.Add(form.portRisk)
	portPan.Add(portSIL)

	portPan.Subscribe(gui.OnResize, func(evname string, ev interface{}) {
		portFL.SetWidth(form.win.ContentWidth())
		// portSL.SetWidth(form.win.ContentWidth())
		// portTL.SetWidth(form.win.ContentWidth())
		// portFOL.SetWidth(form.win.ContentWidth())
		portFIL.SetWidth(form.win.ContentWidth())
		portSIL.SetWidth(form.win.ContentWidth())
	})

	f1 := gui.NewFolder("Portfolio", 500, portPan)
	f1.SetPosition(0, 0)
	f1.SetAlignRight(true)
	form.win.Add(f1)

	// read database values and add them to the form
	prefillPortfolio(form.dropdown)
	form.showPortfolioValues()

	form.win.Dispatch(gui.OnResize, nil)
} // addPortfolio()

// Fill dropdown menu with all portfolio values
func prefillPortfolio(form *gui.DropDown) {
	folios, err := postgresql.FindAllPortfolios()
	misc.ShowError(err, "", "ErrPrint")

	for _, v := range folios {
		// Fill dropdown with available portfolio names
		form.Add(gui.NewImageLabel(v.Name))
	} // for

	// Select the first element and fill the form
	form.SetSelected(form.ItemAt(0))
} // prefillPortfolio ()

func (form *portValue) showPortfolioValues() {
	// Getting value of combo box text
	tmpEntry := form.dropdown.Selected().Text()
	// Get desired portfolio and display the values.
	portfolio, err := postgresql.FindSpecificPortfolio(tmpEntry)
	misc.ShowError(err, "", "ErrPrint")
	form.portID.SetText(fmt.Sprintf("%2d", portfolio.PortfolioID))
	form.tradeRisk.SetText(fmt.Sprintf("%f", portfolio.TradeRisk))
	form.portRisk.SetText(fmt.Sprintf("%f", portfolio.PortRisk))
} // func showPortfolioValues ()
