package gui

import (
	"github.com/g3n/engine/gui"
)

// Handles the creation and actions of the main working panel and its contents
type workingPanel struct {
	panel  *gui.Panel
	tabBar *gui.TabBar
	//tabs   map[string]*gui.Tab // Might be variable in the future
	// The current tabs used (might change in the future)
	mainTab topTab
	portTab portfolioTab
}

// Set up main working panel
func (g *App) addWorkPanel() {
	g.workPanel.panel = gui.NewPanel(0, 0)
	g.workPanel.panel.SetLayoutParams(&gui.DockLayoutParams{Edge: gui.DockCenter})

	// Adding the main tabbar
	g.workPanel.tabBar = gui.NewTabBar(0, 0)
	// Adding main tab
	g.workPanel.mainTab.tab = g.workPanel.tabBar.AddTab("Main")
	g.workPanel.mainTab.addMainTab()
	// Adding portfolio tab
	g.workPanel.portTab.tab = g.workPanel.tabBar.AddTab("Portfolios")
	g.workPanel.portTab.addPortfolioTab()
	g.workPanel.tabBar.SetSelected(0)
	g.workPanel.panel.Add(g.workPanel.tabBar)

	g.workPanel.tabBar.Subscribe(gui.OnResize, func(evname string, ev interface{}) {
		g.workPanel.mainTab.panel.SetPosition(0, g.workPanel.mainTab.tab.Header().Height()+5)
		g.workPanel.mainTab.panel.SetSize(g.workPanel.panel.ContentWidth(), g.workPanel.panel.ContentHeight())
		g.workPanel.portTab.panel.SetPosition(0, g.workPanel.portTab.tab.Header().Height()+5)
		g.workPanel.portTab.panel.SetSize(g.workPanel.panel.ContentWidth(), g.workPanel.panel.ContentHeight())
		g.workPanel.portTab.topPanel.SetPosition(0, 0)
		g.workPanel.portTab.topPanel.SetSize(g.workPanel.panel.ContentWidth(), g.workPanel.panel.ContentHeight())
		g.workPanel.portTab.horSplitter.SetPosition(0, g.workPanel.portTab.tab.Header().Height()+5)
		g.workPanel.portTab.horSplitter.SetSize(g.workPanel.panel.ContentWidth(), g.workPanel.panel.ContentHeight())
	})

	g.workPanel.panel.Subscribe(gui.OnResize, func(evname string, ev interface{}) {
		g.workPanel.tabBar.SetPosition(0, 0)
		g.workPanel.tabBar.SetSize(g.workPanel.panel.ContentWidth(), g.workPanel.panel.ContentHeight())
	})
	g.workPanel.panel.Dispatch(gui.OnResize, nil)
} // addWorkPanel ()
