package gui

import (
	"github.com/g3n/engine/gui"
	"gitlab.com/bluebottle/go-modules/misc"
)

// Build table
func buildTable(cols []gui.TableColumn) (tab *gui.Table) {
	tab, err := gui.NewTable(0, 0, cols)
	misc.ShowError(err, "", "ErrPanic")
	tab.SetBorders(1, 1, 1, 1)
	tab.SetPosition(0, 0)
	tab.SetMargins(1, 1, 25, 1)
	tab.ShowStatus(true)
	return
} // buildTable ()
