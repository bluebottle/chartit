package gui

import (
	"fmt"
	"strconv"

	"github.com/g3n/engine/gui"
	"gitlab.com/bluebottle/go-modules/postgresql"
)

// Structure of symbol list
type symbolList struct {
	symbolWindow *gui.Window // symbol list window
	symbolTable  *gui.Table  // symbol list table
	symbolPanel  *gui.Panel  // symbol list panel
}

func addSymbolList(g *App) {
	g.symbolList.symbolWindow = gui.NewWindow(1000, 275)
	g.symbolList.symbolWindow.SetTitle("Symbols")
	g.symbolList.symbolWindow.SetResizable(true)
	g.symbolList.symbolWindow.SetBorders(5, 5, 5, 5)

	// Adds a new Tab
	g.symbolList.symbolPanel = gui.NewPanel(0, 0)
	sliderBorderColor := guiBlack
	g.symbolList.symbolPanel.SetColor(&sliderBorderColor)
	g.symbolList.symbolPanel.SetRenderable(true)
	g.symbolList.symbolPanel.SetEnabled(true)
	g.symbolList.symbolWindow.Subscribe(gui.OnResize, func(evname string, ev interface{}) {
		g.symbolList.symbolPanel.SetPositionX(0)
		g.symbolList.symbolPanel.SetPositionY(0)
		g.symbolList.symbolPanel.SetSize(g.symbolList.symbolWindow.ContentWidth(), g.symbolList.symbolWindow.ContentHeight())
	})

	// Create table
	g.symbolList.symbolTable = buildTable([]gui.TableColumn{
		{Id: "1", Header: "Symbol", Width: 100, Minwidth: 32, Align: gui.AlignRight, Format: "%s", Resize: true, Sort: gui.TableSortString, Expand: 1},
		{Id: "2", Header: "Type", Width: 100, Minwidth: 32, Align: gui.AlignRight, Format: "%s", Resize: true, Sort: gui.TableSortString, Expand: 1},
		{Id: "3", Header: "Name", Width: 600, Minwidth: 200, Align: gui.AlignRight, Format: "%s", Resize: true, Sort: gui.TableSortString, Expand: 1},
		{Id: "4", Header: "WKN", Width: 100, Minwidth: 32, Align: gui.AlignRight, Format: "%s", Resize: true, Sort: gui.TableSortString, Expand: 1},
		{Id: "5", Header: "ISIN", Width: 100, Minwidth: 32, Align: gui.AlignRight, Format: "%s", Resize: true, Sort: gui.TableSortString, Expand: 1},
		{Id: "6", Header: "Currency", Width: 100, Minwidth: 32, Align: gui.AlignRight, Format: "%s", Resize: true, Sort: gui.TableSortString, Expand: 1},
	})

	// Sets the table data
	g.symbolList.symbolTable.SetRows(genSymbolRows())
	g.symbolList.symbolPanel.Add(g.symbolList.symbolTable)
	g.symbolList.symbolTable.SetSize(g.symbolList.symbolPanel.ContentWidth(), g.symbolList.symbolPanel.ContentHeight())

	//
	// TODO: Fix colum size, resizing on start
	//

	// Subscribe to events to update the table status line
	updateStatus := func(evname string, ev interface{}) {
		g.symbolList.updateStatusLine()
	}
	g.symbolList.symbolTable.Subscribe(gui.OnTableRowCount, updateStatus)
	g.symbolList.symbolTable.Subscribe(gui.OnChange, updateStatus)

	// Resizes table when window resizes
	g.symbolList.symbolPanel.SubscribeID(gui.OnResize, g, func(evname string, ev interface{}) {
		g.symbolList.symbolTable.SetSize(g.symbolList.symbolPanel.ContentWidth(), g.symbolList.symbolPanel.ContentHeight())
	})
	// Checks window position when mouse button is released
	//
	// TODO: Can we prevent windows to be moving outside of the visible area of main window?
	//
	g.symbolList.symbolWindow.SubscribeID(gui.OnMouseUp, g, func(evname string, ev interface{}) {
		//
		// TODO: Works for border but how for window title
		//
		tmpX := g.symbolList.symbolWindow.Position().X
		tmpY := g.symbolList.symbolWindow.Position().Y
		fmt.Printf("%5.2f / %5.2f", tmpX, tmpY)
		if tmpY < 0 {
			tmpY = 0
		} // if

		if tmpX < 0 {
			tmpX = 0
		} // if

		g.symbolList.symbolWindow.SetPosition(tmpX, tmpY)
	})

	// Show status line
	g.symbolList.updateStatusLine()
	g.symbolList.symbolWindow.Add(g.symbolList.symbolPanel)
	g.symbolList.symbolWindow.Dispatch(gui.OnResize, nil)
	g.workPanel.panel.Add(g.symbolList.symbolWindow)
	g.workPanel.panel.Dispatch(gui.OnResize, nil)
} // addSymbolList ()

// Show status line with number of rows, number of selected rows and current row
func (symb symbolList) updateStatusLine() {
	selRows := symb.symbolTable.SelectedRows()
	current := ""

	if len(selRows) > 0 {
		current = strconv.Itoa(selRows[0] + 1)
	} // if

	symb.symbolTable.SetStatusText(fmt.Sprintf("Rows: %d Selected: %d Current: %s", symb.symbolTable.RowCount(), len(selRows), current))
	symb.symbolTable.ShowStatus(true)
} // updateStatusLine()

// Function to generate table data rows
func genSymbolRows() (values []map[string]interface{}) {
	// get all lookup values
	allLookup := postgresql.FindAllCurrentLookup()
	values = make([]map[string]interface{}, 0, len(allLookup))

	for _, v := range allLookup {
		rval := make(map[string]interface{})
		rval["1"] = v.Symbol
		rval["2"] = v.LookType
		rval["3"] = v.Name
		rval["4"] = v.WKN
		rval["5"] = v.ISIN
		rval["6"] = v.QuoteCurrency
		values = append(values, rval)
	} // for

	return
} // genSymbolRows ()
