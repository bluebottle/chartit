package gui

// Handles the creation and actions of the main menu
import (
	"github.com/g3n/engine/gui"
	"github.com/g3n/engine/window"
)

// Set up menu
func addMenu(g *App) {
	// Event handler for menu clicks
	onClick := func(evname string, ev interface{}) {
		tmpOption := ev.(*gui.MenuItem).IdPath()[len(ev.(*gui.MenuItem).IdPath())-1]

		switch tmpOption {
		case "FMivc":
			// Show intrinsic value calculator window
			g.IVC.addIntrinsic(g.statusBar)
			g.workPanel.panel.Add(g.IVC.win)
		case "FMportfolios":
			// Show portfolio window
			g.portfolio.addPortfolio()
			g.workPanel.panel.Add(g.portfolio.win)
		case "FMsymbols":
			// Show symbols window
			addSymbolList(g)
		case "FMtesting":
			// Do testing operations (whithout gui yet)
			testCases(g)
		case "FMquit":
			// Close the window
			g.Application.Exit()
		default:
			g.log.Info("Selected: " + tmpOption)
		} // seclect
	}

	// Create menu bar
	g.menu = gui.NewMenuBar()
	g.menu.Subscribe(gui.OnClick, onClick)
	g.menu.SetLayoutParams(&gui.DockLayoutParams{Edge: gui.DockTop})
	g.menu.SetName("menu")

	// Create File menu and adds it to the menu bar
	fileM := gui.NewMenu()
	// Creates calculators submenu
	m1Calc := gui.NewMenu()
	m1Calc.AddOption("Intrinsic Value Calculator").SetId("FMivc")
	//
	// TODO: Add PSC (Trading/Keep/Lesson\ 5-Position-Sizing-Calculator.xlsx)
	//
	m1Calc.AddOption("Position Size Calculator").SetId("FMpsc")
	fileM.AddMenu("Calculators", m1Calc).SetId("FMcalc")

	fileM.AddOption("Portfolios").SetId("FMportfolios")
	fileM.AddOption("Symbols").SetId("FMsymbols")
	fileM.AddOption("Testing").SetId("FMtesting")
	fileM.AddSeparator()
	fileM.AddOption("Quit").SetId("FMquit")
	g.menu.AddMenu("File", fileM).SetId("file")

	// Create Menu3 and adds it to the menu bar
	m3 := gui.NewMenu()
	m3.AddOption("Help").SetId("HMhelp").SetShortcut(0, window.KeyF1)
	m3.AddOption("Log File").SetId("HMlog")
	m3.AddSeparator()
	//
	// TODO: Add about box
	//
	m3.AddOption("About").SetId("HMabout")
	g.menu.AddMenu("Help", m3).SetId("help")
} // addMenu ()
