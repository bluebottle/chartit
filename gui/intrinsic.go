package gui

import (
	"fmt"
	"strconv"
	"time"

	"github.com/g3n/engine/gui"
	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/bluebottle/go-modules/postgresql"
)

// Handles the creation and actions of the intrinsic value calculator window
type intrinsic struct {
	win           *gui.Window     // window
	name          *gui.Edit       // name of security
	symbol        *gui.Edit       // symbol of security
	countrySelect *gui.DropDown   // dropdown menu for country selection
	loccurSelect  *gui.DropDown   // dropdown menu for local currency selection
	locCurValue   *gui.Label      // Value of current local currency
	poscurSelect  *gui.DropDown   // dropdown menu for currency selection of position
	posCurValue   *gui.Label      // Value of current position currency
	exchangeRate  *gui.Label      // exchange rate to use
	curOpeCash    *gui.Edit       // current operating cashflow
	debt          *gui.Edit       // short + long term debt
	cash          *gui.Edit       // cash + short term investments
	growth15      *gui.Edit       // cash flow growth rate 1-5 years
	growth610     *gui.Edit       // cash flow growth rate 6-10 years
	growth1120    *gui.Edit       // cash flow growth rate 11-20 years
	lfy           *gui.Edit       // last fiscal year
	shares        *gui.Edit       // number of shares outstanding
	beta          *gui.Edit       // beta
	discount      *gui.Label      // discount rate
	yearLabels    [20]*gui.Label  // labels for the year headers
	pcf           [20]*gui.Label  // projected cash flow
	disFac        [20]*gui.Label  // discount factor
	disVal        [20]*gui.Label  // discounted value
	pvSum         *gui.Label      // PV of years cash flow
	intrVal       *gui.Label      // intrinsic value before cash/debt
	debtVal       *gui.Label      // Less debt per share
	cashVal       *gui.Label      // plus cash per share
	finalVal      *gui.Label      // final intrinsic value
	discPremi     *gui.Label      // discount / premium value
	lastClose     *gui.Edit       // last closing value
	yearSwitch10  *gui.CheckRadio // 10 year evaluation
	yearSwitch20  *gui.CheckRadio // 20 year evaluation
	currExchRate  float64         // current exchange rate
}

// Update all fields in the form
func (intr *intrinsic) updateAllFields(pan *gui.Panel) {
	intr.updateDiscountRate()
	intr.updateYears(pan, false, 0, 20)
	intr.updateCashDiscountLabels()
	intr.fillIntrinsicLabel()
	intr.fillDebtLabel()
	intr.fillCashLabel()
	intr.fillFinalValueLabel()
	intr.updateDiscount()
	intr.updateExchangeRate()
} // updateAllFields ()

// Update year fields
func (intr *intrinsic) updateYears(pan *gui.Panel, add bool, start, counter int) {
	tmpYear, err := strconv.Atoi(intr.lfy.Text())
	misc.ShowError(err, "", "ErrFatal")

	for i := start; i < counter; i++ {
		if add {
			intr.yearLabels[i] = setGridLabel(fmt.Sprintf("%d", tmpYear+i+1), 0)
			pan.Add(intr.yearLabels[i])
		} else {
			intr.yearLabels[i].SetText(fmt.Sprintf("%d", tmpYear+i+1))
		} // if
	} // for
} // updateYears ()

// Fill projected cash flow and discount factor labels with the next ten years, starting from current year (or last fiscal year).
func (intr *intrinsic) updateCashDiscountLabels() {
	var oldFieldCF, oldFieldD, tmp15, tmpdiscount, valSum, tmpProj, tmpFac float64
	var counter int

	// get edit field values
	tmp15 = getEditFloat64(intr.growth15)
	tmpdiscount = (1 + getLabelFloat64(intr.discount)/100)
	valSum = 0

	if intr.yearSwitch20.Value() {
		counter = 20
	} else {
		counter = 10
	}

	for i := 0; i < 20; i++ {
		if i < counter {
			if i == 0 {
				oldFieldCF = getEditFloat64(intr.curOpeCash) * (1 + tmp15/100)
				oldFieldD = 1 / tmpdiscount
			} else if i < 5 {
				oldFieldCF = oldFieldCF * (1 + tmp15/100)
				oldFieldD = oldFieldD / tmpdiscount
			} else if i < 10 {
				oldFieldCF = oldFieldCF * (1 + getEditFloat64(intr.growth610)/100)
				oldFieldD = oldFieldD / tmpdiscount
			} else {
				oldFieldCF = oldFieldCF * (1 + getEditFloat64(intr.growth1120)/100)
				oldFieldD = oldFieldD / tmpdiscount
			} // if

			intr.pcf[i].SetText(fmt.Sprintf("%.2f", oldFieldCF))
			intr.disFac[i].SetText(fmt.Sprintf("%.2f", oldFieldD))
			tmpProj = getLabelFloat64(intr.pcf[i])
			tmpFac = getLabelFloat64(intr.disFac[i])
			intr.disVal[i].SetText(fmt.Sprintf("%.2f", tmpProj*tmpFac))
			valSum += tmpProj * tmpFac
		} else {
			intr.pcf[i].SetText(fmt.Sprintf("%.2f", 0.0))
			intr.disFac[i].SetText(fmt.Sprintf("%.2f", 0.0))
			intr.disVal[i].SetText(fmt.Sprintf("%.2f", 0.0))
		} // if
	} // for

	intr.pvSum.SetText(fmt.Sprintf("%.2f", valSum))
} // updateCashFlowLabels ()

// Fill intrinsic value before cash/debt label.
func (intr *intrinsic) fillIntrinsicLabel() {
	value := 0.0
	flowVal := getLabelFloat64(intr.pvSum)
	sharesVal := getEditFloat64(intr.shares)

	if sharesVal != 0.0 {
		value = flowVal / sharesVal
	} // if

	intr.intrVal.SetText(fmt.Sprintf("%.2f", value))
} // fillIntrinsicLabel ()

// Fill debt per share label.
func (intr *intrinsic) fillDebtLabel() {
	value := 0.0
	debtVal := getEditFloat64(intr.debt)
	sharesVal := getEditFloat64(intr.shares)

	if sharesVal != 0.0 {
		value = debtVal / sharesVal
	} // if

	intr.debtVal.SetText(fmt.Sprintf("%.2f", value))
} // fillDebtLabel ()

// Fill cash per share label.
func (intr *intrinsic) fillCashLabel() {
	value := 0.0
	cashVal := getEditFloat64(intr.cash)
	sharesVal := getEditFloat64(intr.shares)

	if sharesVal != 0.0 {
		value = cashVal / sharesVal
	} // if

	intr.cashVal.SetText(fmt.Sprintf("%.2f", value))
} // fillCashLabel ()

// Fill final value per share label.
func (intr *intrinsic) fillFinalValueLabel() {
	var tmpPosTxt, tmpLocTxt string

	intrinsicVal := getLabelFloat64(intr.intrVal)
	debtVal := getLabelFloat64(intr.debtVal)
	cashVal := getLabelFloat64(intr.cashVal)
	tmpIntVal := intrinsicVal - debtVal + cashVal

	// Check if a drop down value is set
	if intr.poscurSelect.SelectedPos() == -1 {
		tmpPosTxt = "EUR"
	} else {
		tmpPosTxt = intr.poscurSelect.Selected().Text()
	} // if

	// Check if a drop down value is set
	if intr.loccurSelect.SelectedPos() == -1 {
		tmpLocTxt = "EUR"
	} else {
		tmpLocTxt = intr.loccurSelect.Selected().Text()
	} // if

	intr.finalVal.SetText(fmt.Sprintf("%.2f %s (%.2f %s)", tmpIntVal, tmpPosTxt, tmpIntVal*intr.currExchRate, tmpLocTxt))
} // fillFinalValueLabel ()

func (intr *intrinsic) updateDiscount() {
	var diff float64
	lastClose := getEditFloat64(intr.lastClose)
	finalVal := getLabelFloat64(intr.finalVal)

	if finalVal != 0.0 {
		diff = (lastClose - finalVal) * 100.0 / finalVal
	} else {
		diff = 0.0
	} // if

	intr.discPremi.SetText(fmt.Sprintf("%.2f%%", diff))
} // updateDiscount()

// Update discount rate
func (intr *intrinsic) updateDiscountRate() {
	// check if we have already selected a country
	if intr.countrySelect.SelectedPos() != -1 {
		MRP, RFR := postgresql.GetRiskValuesForCountry(intr.countrySelect.Selected().Text())
		intr.discount.SetText(fmt.Sprintf("%.5f", RFR+getEditFloat64(intr.beta)*MRP))
	} // if
} // updateDiscountRate()

// Update exchange rate
func (intr *intrinsic) updateExchangeRate() {
	// check if we have already selected a local currency
	if intr.poscurSelect.SelectedPos() != -1 {
		intr.locCurValue.SetText("Exchange Rate 1 " + intr.poscurSelect.Selected().Text() + " is equivalent to")
	} // if

	// check if we have already selected a position currency
	if intr.loccurSelect.SelectedPos() != -1 {
		intr.posCurValue.SetText(intr.loccurSelect.Selected().Text())
	} // if

	// Set exchange rate to 1.00 if one currency is not selected or they are equal, otherwise get latest exchange rate
	if intr.loccurSelect.SelectedPos() == -1 || intr.poscurSelect.SelectedPos() == -1 || intr.poscurSelect.Selected().Text() == intr.loccurSelect.Selected().Text() {
		intr.exchangeRate.SetText("1.00")
	} else {
		intr.currExchRate = postgresql.GetLastQuote(postgresql.GetCurrencyXSymbol(intr.poscurSelect.Selected().Text(), intr.loccurSelect.Selected().Text())).Close
		intr.exchangeRate.SetText(fmt.Sprintf("%f", intr.currExchRate))
	} // if

	//
	// TODO: Somehow do the calculation of the currency changes (only intrinsic values or all fields?)
	//
} // updateExchangeRate()

// Set up intrinsic value calculator window
func (intr *intrinsic) addIntrinsic(stat statBar) {
	intr.win = gui.NewWindow(1050, 450)
	intr.win.SetTitle("Intrinsic Value Calculator (Discounted Cash Flow Method) 10 years")
	ppLayout := gui.NewVBoxLayout()
	ppLayout.SetAutoHeight(true)
	ppLayout.SetAutoWidth(true)
	ppLayout.SetSpacing(2)
	intr.win.SetLayoutParams(ppLayout)
	intr.win.SetResizable(false)

	// Create panel and set it to window size
	IVCPan := gui.NewPanel(0, 0)
	ivcLayoutL := gui.NewGridLayout(11)
	ivcLayoutL.SetExpandH(true)
	//ivcLayoutL.SetExpandV(false)
	IVCPan.SetLayout(ivcLayoutL)
	sliderBorderColor := guiBlack
	IVCPan.SetColor(&sliderBorderColor)
	IVCPan.SetPaddings(2, 2, 2, 2)

	// Create form elements and add them
	IVCPan.Add(setGridLabel("Years to use", 0))
	intr.yearSwitch10 = setGridRadio("10", 0)
	intr.yearSwitch10.SetEnabled(true)
	intr.yearSwitch10.SetGroup("years")
	intr.yearSwitch10.Subscribe(gui.OnClick, func(name string, ev interface{}) {
		intr.win.SetTitle("Intrinsic Value Calculator (Discounted Cash Flow Method) 10 years")
		intr.updateAllFields(IVCPan)
	})
	IVCPan.Add(intr.yearSwitch10)
	intr.yearSwitch20 = setGridRadio("20", 2)
	intr.yearSwitch20.SetEnabled(true)
	intr.yearSwitch20.SetGroup("years")
	intr.yearSwitch20.Subscribe(gui.OnClick, func(name string, ev interface{}) {
		intr.win.SetTitle("Intrinsic Value Calculator (Discounted Cash Flow Method) 20 years")
		intr.updateAllFields(IVCPan)
	})
	IVCPan.Add(intr.yearSwitch20)
	IVCPan.Add(setGridLabel("Country of equity", 3))
	intr.countrySelect = setDropDown("Select...", postgresql.GetAllCountries(), 1)
	intr.countrySelect.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	IVCPan.Add(intr.countrySelect)
	tmpISOs := postgresql.GetAllIsoCodes()
	IVCPan.Add(setGridLabel("Financial statement currency", 0))
	intr.poscurSelect = setDropDown("Select...", tmpISOs, 3)
	intr.poscurSelect.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	IVCPan.Add(intr.poscurSelect)
	IVCPan.Add(setGridLabel("Local currency", 3))
	intr.loccurSelect = setDropDown("Select...", tmpISOs, 1)
	intr.loccurSelect.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	IVCPan.Add(intr.loccurSelect)

	IVCPan.Add(setGridLabel("Name", 0))
	intr.name = setGridEdit(3)
	IVCPan.Add(intr.name)
	IVCPan.Add(setGridLabel("PV of Cash Flows", 3))
	intr.pvSum = setGridLabel("0.00", 1)
	IVCPan.Add(intr.pvSum)

	IVCPan.Add(setGridLabel("Symbol", 0))
	intr.symbol = setGridEdit(3)
	IVCPan.Add(intr.symbol)
	IVCPan.Add(setGridLabel("Intrinsic Value Before Cash/Debt", 3))
	intr.intrVal = setGridLabel("0.00", 1)
	IVCPan.Add(intr.intrVal)

	IVCPan.Add(setGridLabel("Current Operating Cashflow", 0))
	intr.curOpeCash = setGridEdit(3)
	intr.curOpeCash.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	intr.curOpeCash.Subscribe(gui.OnCursor, func(evname string, ev interface{}) {
		stat.text.SetText("in million, if free cash flow > 1.5x net income, use free cash flow, if CFO < 0 => Statement of cash flows last 4 quarters or Net Income < 0 => Latest Annual Income or FCF < 0 => Statement of cash flows last 4 quarters")
	})
	intr.curOpeCash.Subscribe(gui.OnCursorLeave, func(evname string, ev interface{}) {
		stat.text.SetText("")
	})
	IVCPan.Add(intr.curOpeCash)
	IVCPan.Add(setGridLabel("Less Debt Per Share", 3))
	intr.debtVal = setGridLabel("0.00", 1)
	IVCPan.Add(intr.debtVal)

	IVCPan.Add(setGridLabel("Short + Long Term Debt", 0))
	intr.debt = setGridEdit(3)
	intr.debt.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	intr.debt.Subscribe(gui.OnCursor, func(evname string, ev interface{}) {
		stat.text.SetText("in million")
	})
	intr.debt.Subscribe(gui.OnCursorLeave, func(evname string, ev interface{}) {
		stat.text.SetText("")
	})
	IVCPan.Add(intr.debt)
	IVCPan.Add(setGridLabel("Plus Cash Per Share", 3))
	intr.cashVal = setGridLabel("0.00", 1)
	IVCPan.Add(intr.cashVal)

	IVCPan.Add(setGridLabel("Cash + Short Term Investments", 0))
	intr.cash = setGridEdit(3)
	intr.cash.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	intr.cash.Subscribe(gui.OnCursor, func(evname string, ev interface{}) {
		stat.text.SetText("in million")
	})
	intr.cash.Subscribe(gui.OnCursorLeave, func(evname string, ev interface{}) {
		stat.text.SetText("")
	})
	IVCPan.Add(intr.cash)
	IVCPan.Add(setGridLabel("Final Intrinsic Value Per Share", 3))
	intr.finalVal = setGridLabel("0.00 EUR (0.00 EUR)", 1)
	IVCPan.Add(intr.finalVal)

	IVCPan.Add(setGridLabel("Free Cash Flow Growth Rate (1-5 years)", 0))
	intr.growth15 = setGridEdit(3)
	intr.growth15.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	intr.growth15.Subscribe(gui.OnCursor, func(evname string, ev interface{}) {
		stat.text.SetText("long term growth rate or EPS 5-year growth rate")
	})
	intr.growth15.Subscribe(gui.OnCursorLeave, func(evname string, ev interface{}) {
		stat.text.SetText("")
	})
	IVCPan.Add(intr.growth15)
	IVCPan.Add(setGridLabel("Discount/Premium", 3))
	intr.discPremi = setGridLabel("0.00%", 1)
	IVCPan.Add(intr.discPremi)

	IVCPan.Add(setGridLabel("Free Cash Flow Growth Rate (6-10 years)", 0))
	intr.growth610 = setGridEdit(3)
	intr.growth610.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	intr.growth610.Subscribe(gui.OnCursor, func(evname string, ev interface{}) {
		stat.text.SetText("half of 1-5y, maximum 15%")
	})
	intr.growth610.Subscribe(gui.OnCursorLeave, func(evname string, ev interface{}) {
		stat.text.SetText("")
	})
	IVCPan.Add(intr.growth610)
	IVCPan.Add(setGridLabel("Discount Rate %", 3))
	intr.discount = setGridLabel("0.00", 1)
	IVCPan.Add(intr.discount)

	IVCPan.Add(setGridLabel("Free Cash Flow Growth Rate (11-20 years)", 0))
	intr.growth1120 = setGridEdit(3)
	intr.growth1120.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	intr.growth1120.Subscribe(gui.OnCursor, func(evname string, ev interface{}) {
		stat.text.SetText("half of 6-10y, maximum 10%")
	})
	intr.growth1120.Subscribe(gui.OnCursorLeave, func(evname string, ev interface{}) {
		stat.text.SetText("")
	})
	IVCPan.Add(intr.growth1120)

	if intr.loccurSelect.SelectedPos() != -1 {
		intr.locCurValue = setGridLabel("Exchange Rate 1 "+intr.poscurSelect.Selected().Text()+" is equivalent to", 3)
		intr.posCurValue = setGridLabel(intr.loccurSelect.Selected().Text(), 1)
	} else {
		intr.locCurValue = setGridLabel("Exchange Rate 1 EUR is equivalent to", 3)
		intr.posCurValue = setGridLabel("EUR", 1)
	} // if

	IVCPan.Add(intr.locCurValue)
	intr.currExchRate = 1.00
	intr.exchangeRate = setGridLabel(fmt.Sprintf ("%.2f", intr.currExchRate), 0)
	IVCPan.Add(intr.exchangeRate)
	IVCPan.Add(intr.posCurValue)
	IVCPan.Add(setGridLabel("Equity beta", 0))
	intr.beta = setGridEdit(3)
	intr.beta.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	IVCPan.Add(intr.beta)
	IVCPan.Add(setGridLabel("Last Close", 3))
	intr.lastClose = setGridEdit(1)
	intr.lastClose.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	intr.lastClose.Subscribe(gui.OnCursor, func(evname string, ev interface{}) {
		stat.text.SetText("last closing value")
	})
	intr.lastClose.Subscribe(gui.OnCursorLeave, func(evname string, ev interface{}) {
		stat.text.SetText("")
	})
	IVCPan.Add(intr.lastClose)

	IVCPan.Add(setGridLabel("Number of shares outstanding", 0))
	intr.shares = setGridEdit(3)
	intr.shares.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	intr.shares.Subscribe(gui.OnCursor, func(evname string, ev interface{}) {
		stat.text.SetText("in million")
	})
	intr.shares.Subscribe(gui.OnCursorLeave, func(evname string, ev interface{}) {
		stat.text.SetText("")
	})
	IVCPan.Add(intr.shares)
	IVCPan.Add(setGridLabel("Last Fiscal Year", 3))
	intr.lfy = setGridEdit(5)
	// get current year as default value
	t := time.Now()
	intr.lfy.SetText(fmt.Sprintf("%d", t.Year()))
	intr.lfy.Subscribe(gui.OnCursor, func(evname string, ev interface{}) {
		stat.text.SetText("four digit year")
	})
	intr.lfy.Subscribe(gui.OnCursorLeave, func(evname string, ev interface{}) {
		stat.text.SetText("")
	})
	intr.lfy.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		intr.updateAllFields(IVCPan)
	})
	IVCPan.Add(intr.lfy)

	IVCPan.Add(setGridLabel("Year", 0))
	// Read value from last fiscal year field and start from there
	intr.updateYears(IVCPan, true, 0, 10)

	IVCPan.Add(setGridLabel("Projected Cash Flow", 0))

	for i := 0; i < 10; i++ {
		intr.pcf[i] = setGridLabel("0.00", 0)
		IVCPan.Add(intr.pcf[i])
	} // for

	IVCPan.Add(setGridLabel("Discount Factor", 0))

	for i := 0; i < 10; i++ {
		intr.disFac[i] = setGridLabel("0.00", 0)
		IVCPan.Add(intr.disFac[i])
	} // for

	IVCPan.Add(setGridLabel("Discounted Value", 0))

	for i := 0; i < 10; i++ {
		intr.disVal[i] = setGridLabel("0.00", 0)
		IVCPan.Add(intr.disVal[i])
	} // for

	IVCPan.Add(setGridLabel("Year", 0))
	// Read value from last fiscal year field and start from there
	intr.updateYears(IVCPan, true, 10, 20)

	IVCPan.Add(setGridLabel("Projected Cash Flow", 0))

	for i := 10; i < 20; i++ {
		intr.pcf[i] = setGridLabel("0.00", 0)
		IVCPan.Add(intr.pcf[i])
	} // for

	IVCPan.Add(setGridLabel("Discount Factor", 0))

	for i := 10; i < 20; i++ {
		intr.disFac[i] = setGridLabel("0.00", 0)
		IVCPan.Add(intr.disFac[i])
	} // for

	IVCPan.Add(setGridLabel("Discounted Value", 0))

	for i := 10; i < 20; i++ {
		intr.disVal[i] = setGridLabel("0.00", 0)
		IVCPan.Add(intr.disVal[i])
	} // for

	intr.win.Subscribe(gui.OnResize, func(evname string, ev interface{}) {
		IVCPan.SetPositionX(0)
		IVCPan.SetPositionY(0)
		IVCPan.SetSize(intr.win.ContentWidth(), intr.win.ContentHeight())
	})

	intr.win.Add(IVCPan)
	intr.win.Dispatch(gui.OnResize, nil)
} // addIntrinsic ()

// Update is called every frame.
func (t *intrinsic) Update(a *App, deltaTime time.Duration) {}
