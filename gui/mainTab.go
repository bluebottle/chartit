package gui

import (
	"github.com/g3n/engine/gui"
	"gitlab.com/bluebottle/go-modules/postgresql"
	"gitlab.com/bluebottle/go-modules/technical"
)

// Handles the creation and actions of the main tab of the working panel and its contents
type topTab struct {
	panel *gui.Panel
	tab   *gui.Tab
}

func (mt *topTab) addMainTab() {
	mt.panel = gui.NewPanel(0, 0)
	mt.tab.SetPinned(true)
	bl1 := gui.NewVBoxLayout()
	bl1.SetSpacing(4)
	mt.panel.SetLayout(bl1)

	// Create table
	tmpTab := buildTable([]gui.TableColumn{
		{Id: "1", Header: "Name", Width: 250, Minwidth: 250, Align: gui.AlignRight, Format: "%s", Expand: 0},
		{Id: "2", Header: "Daily Stochastic", Width: 100, Minwidth: 100, Align: gui.AlignRight, Format: "%3.2f", Expand: 0},
		{Id: "3", Header: "Weekly Stochastic", Width: 100, Minwidth: 100, Align: gui.AlignRight, Format: "%3.2f", Expand: 0},
		{Id: "4", Header: "Monthly Stochastic", Width: 100, Minwidth: 100, Align: gui.AlignRight, Format: "%3.2f", Expand: 0},
		{Id: "5", Header: "Yearly Stochastic", Width: 100, Minwidth: 100, Align: gui.AlignRight, Format: "%3.2f", Expand: 0},
	})

	// Sets the table data
	mt.panel.Add(tmpTab)
	tmpTab.SetSize(mt.panel.ContentWidth(), mt.panel.ContentHeight())
	// Resizes table when window resizes
	mt.panel.SubscribeID(gui.OnResize, mt, func(evname string, ev interface{}) {
		tmpTab.SetSize(mt.panel.ContentWidth(), mt.panel.ContentHeight())
	})

	// TODO: Let the user add values instead of being hardcoded
	tmpTab.AddRow(addSymbolOverview("DE0008469008")) // Add Dax
	tmpTab.AddRow(addSymbolOverview("US2605661048")) // Add DJI
	tmpTab.AddRow(addSymbolOverview("US78378X1072")) // Add S&P
	mt.tab.SetContent(mt.panel)
} // addMainTab ()

// Get lookup values for given ISIN and return statistical values when found. Otherwise return an error message.
func addSymbolOverview(isin string) (portFL map[string]interface{}) {
	tmpLookup := postgresql.GetSingleIsinLookup(isin)
	portFL = make(map[string]interface{})

	// check if we found an entry in the database
	if tmpLookup.ISIN == isin {
		tmpEval := technical.CompleteCurrentStochastic(tmpLookup.Symbol, 14, 5, 5, 20.0, 25.0, 100.0, 100.0)
		portFL["1"] = tmpLookup.Name
		portFL["2"] = tmpEval["D"].CurrentValue
		portFL["3"] = tmpEval["W"].CurrentValue
		portFL["4"] = tmpEval["M"].CurrentValue
		portFL["5"] = tmpEval["Y"].CurrentValue

		quoteVal := postgresql.FindQuotes(tmpLookup.Symbol, "asc")
		tmpEMA := technical.EmaDiffPer(technical.EMA(50, postgresql.GetClose(quoteVal)), quoteVal)
		portFL["6"] = tmpEMA
	} else {
		portFL["1"] = "ISIN " + isin + " not found"
	} // if

	return
} // addSymbolOverview()
