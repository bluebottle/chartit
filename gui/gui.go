package gui

import (
	"runtime"
	"time"

	"github.com/g3n/engine/app"
	"github.com/g3n/engine/camera"
	"github.com/g3n/engine/core"
	"github.com/g3n/engine/gls"
	"github.com/g3n/engine/gui"
	"github.com/g3n/engine/light"
	"github.com/g3n/engine/math32"
	"github.com/g3n/engine/renderer"
	"github.com/g3n/engine/util"
	"github.com/g3n/engine/util/logger"
	"github.com/g3n/engine/util/stats"
	"github.com/g3n/engine/window"
	"github.com/knadh/koanf/v2"

	"gitlab.com/bluebottle/chartit/configuration"
	"gitlab.com/bluebottle/go-modules/misc"
	"gitlab.com/bluebottle/go-modules/postgresql"
)

var (
	version    = "0.0.1"
	appName    = "chartit"
	configFile = appName + ".yml"
	configPath = "/etc/" + appName
)

// App contains the application state
type App struct {
	*app.Application                // Embedded standard application object
	log              *logger.Logger // Application logger
	stats            *stats.Stats   // statistics object
	gs               *gls.GLS
	frameRater       *util.FrameRater // Render loop frame rater
	camera           *camera.Camera
	orbit            *camera.OrbitControl
	scene            *core.Node

	main       *gui.Panel
	menu       *gui.Menu
	statusBar  statBar
	workPanel  workingPanel
	watchList  watchListBar
	symbolList symbolList

	portfolio portValue // portfolio window
	IVC       intrinsic // intrinsic value calculator window
	config    *koanf.Koanf
}

// SetupGui creates all user interface elements
func (g *App) SetupGui(width, height int) {
	g.log.Info("Creating GUI...")
	gmLayout := gui.NewDockLayout()
	// Create main panel and set it to root size
	g.main = gui.NewPanel(float32(width), float32(height))
	g.main.SetRenderable(false)
	g.main.SetEnabled(false)
	g.main.SetLayout(gmLayout)
	g.scene.Add(g.main)
	gui.Manager().Set(g.main)
	sliderBorderColor := guiBrown
	g.main.SetBordersColor4(&sliderBorderColor)
	g.main.SetColor4(&gui.StyleDefault().Scroller.BgColor)

	addMenu(g)
	g.main.Add(g.menu)

	g.addWorkPanel()
	g.main.Add(g.workPanel.panel)

	g.statusBar.addStatusbar()
	g.main.Add(g.statusBar.pan)

	g.log.Info("Done creating GUI.")
} // SetupGui ()

// Create Creates the app
func Create() (a *App) {
	// Create ChartIt struct
	a = new(App)
	a.Application = app.App(1900, 1000, appName+" v"+version)
	// OpenGL functions must be executed in the same thread where
	// the context was created (by window.New())
	runtime.LockOSThread()

	// Create logger
	a.log = logger.New("ChartIt", nil)
	a.log.AddWriter(logger.NewConsole(false))
	a.log.SetFormat(logger.FTIME | logger.FMICROS)
	a.log.SetLevel(logger.INFO)
	a.log.Info("%s v%s starting", appName, version)

	a.stats = stats.NewStats(a.Gls())

	// Log OpenGL version
	glVersion := a.gs.GetString(gls.VERSION)
	a.log.Info("OpenGL version: %s", glVersion)

	// Turn off OpenGL error checking
	a.Gls().SetCheckErrors(false)

	// Read config file.
	a.config = configuration.ReadProgConfig(configPath + "/" + configFile)
	// open database connection
	err := postgresql.NewDBConnection(
		a.config.String("database.user"),
		a.config.String("database.host"),
		a.config.String("database.password"),
		a.config.String("database.port"),
		a.config.String("database.dbname"),
		appName,
		a.config.String("database.sslmode"))
	misc.ShowError(err, "Could not open postgres database.", "ErrMsgFatal")

	// Create scenes
	a.scene = core.NewNode()

	width, height := a.GetSize()
	aspect := float32(width) / float32(height)
	a.camera = camera.New(aspect)
	a.scene.Add(a.camera)
	a.orbit = camera.NewOrbitControl(a.camera)

	// Add white ambient light to the scene
	ambLight := light.NewAmbient(&guiWhite, 0.4)
	a.scene.Add(ambLight)

	// Create frame rater
	a.frameRater = util.NewFrameRater(60)
	// Setup gui
	a.SetupGui(width, height)

	// Sets the default window resize event handler
	a.Subscribe(window.OnWindowSize, func(evname string, ev interface{}) { a.OnWindowResize() })
	a.OnWindowResize()

	// Reset camera
	a.camera.SetPosition(0, 0, 5)
	a.camera.UpdateSize(5)
	a.camera.LookAt(&math32.Vector3{X: 0, Y: 0, Z: 0}, &math32.Vector3{X: 0, Y: 1, Z: 0})
	a.camera.SetProjection(camera.Perspective)
	a.orbit.Reset()

	return
} // App ()

func (a *App) Run() {
	a.Application.Run(a.Update)
} // Run()

func (a *App) Update(rend *renderer.Renderer, deltaTime time.Duration) {
	// Start measuring this frame
	a.frameRater.Start()
	// Clear the color, depth, and stencil buffers
	a.Gls().Clear(gls.COLOR_BUFFER_BIT | gls.DEPTH_BUFFER_BIT | gls.STENCIL_BUFFER_BIT)

	// Render scene
	err := rend.Render(a.scene, a.camera)
	misc.ShowError(err, "", "ErrPanic")

	// Update GUI timers
	gui.Manager().TimerManager.ProcessTimers()

	// Control and update FPS
	a.frameRater.Wait()
	//	a.updateFPS()
} // Update()

// OnWindowResize is default handler for window resize events.
func (a *App) OnWindowResize() {
	// Get framebuffer size and set the viewport accordingly
	width, height := a.GetFramebufferSize()
	a.Gls().Viewport(0, 0, int32(width), int32(height))

	// Set camera aspect ratio
	a.camera.SetAspect(float32(width) / float32(height))
	a.main.SetSize(float32(width), float32(height))
} // OnWindowResize()
