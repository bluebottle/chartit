package gui

import (
	"github.com/g3n/engine/gui"
)

type portInfoFolder struct {
	folder *gui.Folder // Folder for the info of the selected portfolio
	//
	// TODO: Add fields for the portfolio info folder
	//
}

func (pIF *portInfoFolder) addInfoFolder() {
	pIF.folder = gui.NewFolder("Portfolio info", 150, gui.NewLabel("Left"))
	pIF.folder.SetPosition(0, 0)
	pIF.folder.SetAlignRight(true)
} // addInfoFolder()
