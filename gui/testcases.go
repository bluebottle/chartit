package gui

import (
	"fmt"

	"github.com/g3n/engine/gui"
	"gitlab.com/bluebottle/go-modules/misc"
)

// Structure of all watch lists
type watchListBar struct {
	watchWindow   *gui.Window // watch list window
	watchTabBar   *gui.TabBar // watch list tab bar
	watchLists    []watchList // slice of defined watch lists
	watchBarPanel *gui.Panel  // watch list panel
}

// Structure of individual watch lists
type watchList struct {
	watchTab   *gui.Tab   // watch list tab
	watchPanel *gui.Panel // watch list panel
	watchTable *gui.Table // watch list table
}

// Fill the given tab with the watchlist
func newWatchlist(g *App, title string) watchList {
	var (
		err error
		ok  bool
	)
	tmpWL := watchList{watchTab: g.watchList.watchTabBar.AddTab(title)}
	tmpWL.watchTab.SetContent(gui.NewPanel(0, 0))
	tmpWL.watchPanel, ok = tmpWL.watchTab.Content().(*gui.Panel)

	if !ok {
		misc.ShowError(nil, "Could not create panel", "MsgFatal")
	} // if

	sliderBorderColor := guiBrown
	tmpWL.watchPanel.SetBordersColor4(&sliderBorderColor)

	// Create table
	//
	// TODO: Create watch lists with stored columns?
	//
	tmpWL.watchTable, err = gui.NewTable(0, 0, []gui.TableColumn{
		{Id: "1", Header: "Col1", Width: 100, Minwidth: 32, Align: gui.AlignLeft, Format: "%d", Resize: true, Sort: gui.TableSortNumber, Expand: 0},
		// {Id: "2", Header: "Col2", Width: 100, Minwidth: 32, Align: gui.AlignCenter, FormatFunc: formatTime, Resize: true, Expand: 0},
		// {Id: "3", Header: "Col3", Width: 100, Minwidth: 32, Align: gui.AlignRight, Format: "US$%6.2f", Resize: true, Expand: 0},
		// {Id: "4", Header: "Col4", Width: 100, Minwidth: 32, Align: gui.AlignCenter, FormatFunc: formatCalc, Expand: 0},
	})
	misc.ShowError(err, "", "ErrPanic")

	// Sets the table data
	tmpWL.watchTable.SetRows(genRows(15))
	tmpWL.watchTable.SetBorders(1, 1, 1, 1)
	tmpWL.watchTable.SetPosition(0, 0)
	tmpWL.watchTable.SetMargins(0, 0, 0, 0)

	tmpWL.watchPanel.Add(tmpWL.watchTable)

	return tmpWL
} // newWatchlist ()

func testCases(g *App) {
	fmt.Println("Testcases")
	g.watchList.watchWindow = gui.NewWindow(1000, 275)
	g.watchList.watchWindow.SetTitle("Watchlists")
	ppLayout := gui.NewVBoxLayout()
	ppLayout.SetAutoHeight(true)
	ppLayout.SetAutoWidth(true)
	ppLayout.SetSpacing(0)
	g.watchList.watchWindow.SetLayoutParams(ppLayout)
	g.watchList.watchWindow.SetResizable(true)

	// Add watch bar panel
	g.watchList.watchBarPanel = gui.NewPanel(0, 0)
	sliderBorderColor := guiBlack
	g.watchList.watchBarPanel.SetColor(&sliderBorderColor)
	g.watchList.watchBarPanel.SetLayout(ppLayout)
	g.watchList.watchBarPanel.SetRenderable(true)
	g.watchList.watchBarPanel.SetEnabled(true)
	g.watchList.watchWindow.Subscribe(gui.OnResize, func(evname string, ev interface{}) {
		g.watchList.watchBarPanel.SetPositionX(0)
		g.watchList.watchBarPanel.SetPositionY(0)
		g.watchList.watchBarPanel.SetSize(g.watchList.watchWindow.ContentWidth(), g.watchList.watchWindow.ContentHeight())
	})

	// Creates TabBar
	g.watchList.watchTabBar = gui.NewTabBar(0, 0)
	g.watchList.watchTabBar.SetPosition(0, 0)
	g.watchList.watchBarPanel.Add(g.watchList.watchTabBar)
	g.watchList.watchBarPanel.Subscribe(gui.OnResize, func(evname string, ev interface{}) {
		g.watchList.watchTabBar.SetPositionX(0)
		g.watchList.watchTabBar.SetPositionY(0)
		g.watchList.watchTabBar.SetSize(g.watchList.watchBarPanel.ContentWidth(), g.watchList.watchBarPanel.ContentHeight())
	})

	// Resizes TabBar and table when main window resizes
	g.watchList.watchBarPanel.SubscribeID(gui.OnResize, g, func(evname string, ev interface{}) {
		g.watchList.watchTabBar.SetSize(g.watchList.watchTabBar.ContentWidth(), g.watchList.watchTabBar.ContentHeight())
		g.watchList.watchLists[0].watchTable.SetSize(g.watchList.watchLists[0].watchPanel.ContentWidth(), g.watchList.watchLists[0].watchPanel.ContentHeight())
	})

	// Adds a new Tab
	tabText := fmt.Sprintf("Tab: (%s)", "Test")
	g.watchList.watchLists = append(g.watchList.watchLists, newWatchlist(g, tabText))

	g.watchList.watchWindow.Add(g.watchList.watchBarPanel)
	g.watchList.watchWindow.Dispatch(gui.OnResize, nil)
	g.workPanel.panel.Add(g.watchList.watchWindow)
	g.workPanel.panel.Dispatch(gui.OnResize, nil)
} // testCases ()

// Function to generate table data rows
//
// TODO: Fill the defined columns for the given watch list
func genRows(count int) (values []map[string]interface{}) {
	nextRow := 0
	values = make([]map[string]interface{}, 0, count)

	for i := 0; i < count; i++ {
		rval := make(map[string]interface{})
		rval["1"] = nextRow // column id
		// rval["2"] = time.Now()
		// rval["3"] = float64(nextRow) / 3
		// rval["4"] = nil
		values = append(values, rval)
		nextRow++
	} // for

	return
} // genRows ()
